#include <SDL2/SDL.h>
#undef main

#include "gl.hpp"

#ifdef __APPLE__
#include <al.h>
#include <alc.h>
#else
#include <AL/al.h>
#include <AL/alc.h>
#endif

#include <memory>
#include <array>
#include <vector>
#include <chrono>
#include <string>
#include <iomanip>
#include <deque>
#include <set>
#include <stdexcept>
#include <sstream>
#include <iostream>
#include <fstream>
#include <random>
#include <thread>
#include <optional>
#include <unordered_map>
#include <functional>

static constexpr float pi = 3.141592653589793f;

template <typename T>
T lerp(T const & a, T const & b, float t)
{
	return a + (b - a) * t;
}

float smoothstep(float t)
{
	return t * t * (3.f - 2.f * t);
}

template <typename T>
T slerp(T const & a, T const & b, float t)
{
	return lerp(a, b, smoothstep(t));
}

template <typename T>
T clamp(T const & x, T const & min, T const & max)
{
	return std::min(max, std::max(min, x));
}

template <typename T>
void update (T & value, T const & target, float dt)
{
	value += (target - value) * dt;
}

// boost::hash_combine
void hash_combine(std::size_t & h1, std::size_t h2)
{
	h1 ^= h2 + 0x9e3779b9 + (h1<<6) + (h1>>2);
}

template <typename ... Args>
std::string to_string(Args const & ... args)
{
	std::ostringstream os;
	(os << ... << args);
	return os.str();
}

template <typename ... Args>
[[noreturn]] void fail(Args const & ... args)
{
	throw std::runtime_error(to_string(args...));
}

template <typename T>
using sdl_ptr = std::unique_ptr<T, void(*)(T*)>;

[[noreturn]] void sdl_fail(std::string const & message)
{
	fail(message, SDL_GetError());
}

template <typename F>
struct at_scope_exit
{
	F f;

	~at_scope_exit()
	{
		f();
	}
};

template <typename F>
at_scope_exit(F) -> at_scope_exit<F>;

struct vec2
{
	float x, y;

	vec2 & operator *= (float s);
	vec2 & operator /= (float s);

	vec2 & operator += (vec2 const & v);
	vec2 & operator -= (vec2 const & v);
};

vec2 & vec2::operator *= (float s)
{
	x *= s;
	y *= s;
	return *this;
}

vec2 & vec2::operator /= (float s)
{
	x /= s;
	y /= s;
	return *this;
}

vec2 & vec2::operator += (vec2 const & v)
{
	x += v.x;
	y += v.y;
	return *this;
}

vec2 & vec2::operator -= (vec2 const & v)
{
	x -= v.x;
	y -= v.y;
	return *this;
}

vec2 operator * (vec2 const & v, float s)
{
	return { v.x * s, v.y * s };
}

vec2 operator * (float s, vec2 const & v)
{
	return { s * v.x, s * v.y };
}

vec2 operator / (vec2 const & v, float s)
{
	return { v.x / s, v.y / s };
}

vec2 operator - (vec2 const & v)
{
	return { -v.x, -v.y };
}

vec2 operator + (vec2 const & v, vec2 const & u)
{
	return { v.x + u.x, v.y + u.y };
}

vec2 operator - (vec2 const & v, vec2 const & u)
{
	return { v.x - u.x, v.y - u.y };
}

vec2 dir(float a)
{
	return { std::cos(a), std::sin(a) };
}

float dot(vec2 const & v, vec2 const & u)
{
	return v.x * u.x + v.y * u.y;
}

float length(vec2 const & v)
{
	return std::sqrt(dot(v, v));
}

float distance(vec2 const & v, vec2 const & u)
{
	return length(v - u);
}

vec2 normalized(vec2 const & v)
{
	return v / length(v);
}

vec2 rot90(vec2 const & v)
{
	return { -v.y, v.x };
}

float det(vec2 const & v, vec2 const & u)
{
	return v.x * u.y - v.y * u.x;
}

std::ostream & operator<< (std::ostream & os, vec2 const & v)
{
	return os << '(' << v.x << ", " << v.y << ')';
}

struct color
{
	std::uint8_t r, g, b, a;
};

struct random_engine
{
	random_engine()
		: rng_(std::random_device{}())
	{}

	// [0, 1)
	float operator() ()
	{
		return (*this)(0.f, 1.f);
	}

	float operator() (float from, float to)
	{
		return std::uniform_real_distribution<float>{from, to}(rng_);
	}

	int integer(int n)
	{
		return std::uniform_int_distribution<int>{0, n - 1}(rng_);
	}

	vec2 unit_vector()
	{
		vec2 v;
		while (true)
		{
			v.x = (*this)(-1.f, 1.f);
			v.y = (*this)(-1.f, 1.f);

			auto d = dot(v, v);
			if (d <= 1.f)
				return v / std::sqrt(d);
		}
	}

private:
	std::default_random_engine rng_;

	std::uniform_real_distribution<float> unit_d_{0.f, 1.f};
};

struct sound
{
	sound(random_engine & rng);

	void play_bullet() { alSourcePlay(bullet_.source); }
	void play_explosion();
	void play_propulsion() { alSourcePlay(propulsion_.source); }

	void pitch(float p);

	void propulsion_level(float p);

private:
	random_engine & rng_;

	ALCdevice * device_;
	ALCcontext * context_;

	static constexpr int sample_rate = 44100;

	struct track
	{
		ALuint source, buffer;
	};

	struct multitrack
	{
		using clock = std::chrono::high_resolution_clock;

		ALuint buffer;
		std::vector<ALuint> sources;
		std::vector<std::optional<clock::time_point>> end_time;
	};

	float pitch_ = 1.f;
	float propulsion_level_ = 0.f;

	track bullet_;
	multitrack explosion_;
	track propulsion_;

	void setup_source(ALuint source);
	track make_track();

	track gen_bullet();
	track gen_explosion();
	track gen_propulsion();

	template <typename Freq, typename Ampl>
	std::vector<std::int16_t> sine(int sample_count, Freq freq, Ampl ampl);

	template <typename Freq, typename Ampl>
	std::vector<std::int16_t> noise(int sample_count, Freq freq, Ampl ampl, int filter = 0);
};

sound::sound(random_engine & rng)
	: rng_(rng)
{
	alGetError();

	device_ = alcOpenDevice(nullptr);
	if (!device_) fail("Failed to create OpenAL device");

	context_ = alcCreateContext(device_, nullptr);
	if (!context_) fail("Failed to create OpenAL context");
	if (!alcMakeContextCurrent(context_)) fail("Failed to make OpenAL context current");

	{
		float orientation[6] = { 0.f, 0.f, 1.f, 0.f, 1.f, 0.f };
		alListener3f(AL_POSITION, 0.f, 0.f, 1.f);
		alListener3f(AL_VELOCITY, 0.f, 0.f, 0.f);
		alListenerfv(AL_ORIENTATION, orientation);
	}

	bullet_ = gen_bullet();
	propulsion_ = gen_propulsion();

	{
		auto explosion = gen_explosion();

		int const max_explosions = 20;

		explosion_.buffer = explosion.buffer;
		explosion_.sources.resize(max_explosions);
		explosion_.end_time.resize(max_explosions);

		alGenSources(explosion_.sources.size(), explosion_.sources.data());
		for (int source : explosion_.sources)
		{
			setup_source(source);
			alSourcei(source, AL_BUFFER, explosion_.buffer);
		}
	}

	alSourcei(propulsion_.source, AL_LOOPING, AL_TRUE);
	alSourcePlay(propulsion_.source);

	if (auto e = alGetError(); e != AL_NO_ERROR)
		fail("alGetError(): ", e);
}

void sound::play_explosion()
{
	auto const now = multitrack::clock::now();

	auto const duration = std::chrono::milliseconds{500};

	for (int i = 0; i < explosion_.sources.size(); ++i)
	{
		if (explosion_.end_time[i] && *explosion_.end_time[i] < now)
		{
			explosion_.end_time[i] = std::nullopt;
		}

		if (!explosion_.end_time[i])
		{
			explosion_.end_time[i] = now + duration;
			alSourcePlay(explosion_.sources[i]);
			break;
		}
	}
}

void sound::setup_source(ALuint source)
{
	alSourcef(source, AL_PITCH, 1.f);
	alSourcef(source, AL_GAIN, 1.f);
	alSource3f(source, AL_POSITION, 0.f, 0.f, 0.f);
	alSource3f(source, AL_VELOCITY, 0.f, 0.f, 0.f);
	alSourcei(source, AL_LOOPING, AL_FALSE);
}

sound::track sound::make_track()
{
	track t;

	alGenSources(1, &t.source);
	setup_source(t.source);

	alGenBuffers(1, &t.buffer);

	return t;
}

sound::track sound::gen_bullet()
{
	auto t = make_track();

	auto freq = [](float t){ return lerp(1600.f, 200.f, t); };
	auto ampl = [](float t){
		auto s = [](float a){ return a*a*(3.f-2.f*a); };

		float const attack = 0.01f;

		if (t < attack)
			return s(t / attack);

		float a = (1.f - t) / (1.f - attack);
		return s(a);
	};

	auto data = sine(sample_rate / 4, freq, ampl);

	alBufferData(t.buffer, AL_FORMAT_MONO16, data.data(), data.size() * sizeof(data[0]), sample_rate);
	alSourcei(t.source, AL_BUFFER, t.buffer);

	return t;
}

sound::track sound::gen_explosion()
{
	auto t = make_track();

	auto freq = [](float t){ return lerp(400.f, 100.f, std::pow(t, 1.f / 3.f)); };
	auto ampl = [](float t){
		auto s = [](float a){ return a*a*(3.f-2.f*a); };

		float const A = 0.3f;

		float const attack = 0.1f;

		if (t < attack)
			return A * s(t / attack);

		float a = (1.f - t) / (1.f - attack);
		return A * s(a);
	};

	auto data = noise(sample_rate / 2, freq, ampl, 100);

	alBufferData(t.buffer, AL_FORMAT_MONO16, data.data(), data.size() * sizeof(data[0]), sample_rate);
	alSourcei(t.source, AL_BUFFER, t.buffer);

	return t;
}

sound::track sound::gen_propulsion()
{
	auto t = make_track();

	auto freq = [](float t){ return 60.f; };
	auto ampl = [](float t){ return 1.f; };

	auto data = noise(sample_rate * 16, freq, ampl, 200);

	alBufferData(t.buffer, AL_FORMAT_MONO16, data.data(), data.size() * sizeof(data[0]), sample_rate);
	alSourcei(t.source, AL_BUFFER, t.buffer);

	return t;
}

void sound::pitch(float p)
{
	pitch_ = p;
	alSourcef(bullet_.source, AL_PITCH, p);
	for (int source : explosion_.sources)
		alSourcef(source, AL_PITCH, p);
	alSourcef(propulsion_.source, AL_PITCH, p);
}

void sound::propulsion_level(float p)
{
	alSourcef(propulsion_.source, AL_GAIN, p);

	propulsion_level_ = p;
}

template <typename Freq, typename Ampl>
std::vector<std::int16_t> sound::sine(int sample_count, Freq freq, Ampl ampl)
{
	std::vector<std::int16_t> data(sample_count);

	float phase = 0.f;

	for (int i = 0; i < sample_count; ++i)
	{
		float const t = (i * 1.f) / sample_count;

		phase += 2.f * M_PI * freq(t) / sample_rate;

		while (phase > 2.f * M_PI)
			phase -= 2.f * M_PI;

		float value = ampl(t) * std::sin(phase);

		data[i] = std::round((1.f + clamp(value, -1.f, 1.f)) / 2.f * 65535) - 32768;
	}

	return data;
}

template <typename Freq, typename Ampl>
std::vector<std::int16_t> sound::noise(int sample_count, Freq freq, Ampl ampl, int filter)
{
	float phase = 0.f;

	int seed = -1;
	float amplitude = 0.f;

	std::vector<float> values(sample_count);

	for (int i = 0; i < sample_count; ++i)
	{
		float const t = (i * 1.f) / sample_count;

		phase += 2.f * M_PI * freq(t) / sample_rate;

		int new_seed = std::floor(phase / 2.f * M_PI);
		if (new_seed != seed)
		{
			seed = new_seed;
			amplitude = rng_(-1.f, 1.f);
		}

		values[i] = amplitude * ampl(t);
	}

	for (int s = 0; s < filter; ++s)
	{
		std::vector<float> new_values(sample_count);
		for (int i = 0; i < sample_count; ++i)
		{
			if (i == 0 || (i + 1) == sample_count)
				new_values[i] = values[i];
			else
				new_values[i] = 0.25f * (values[i - 1] + 2.f * values[i] + values[i + 1]);
		}
		values = std::move(new_values);
	}

	std::vector<std::int16_t> data(sample_count);

	for (int i = 0; i < sample_count; ++i)
		data[i] = 32767 * clamp(values[i], -1.f, 1.f);


	return data;
}

struct mesh_attribute_data
{
	GLuint dimension;
	GLenum type;
	bool normalized;
	GLsizei size; // in bytes
};

template <typename T>
mesh_attribute_data attr();

template <>
mesh_attribute_data attr<float>()
{
	return { 1, gl::FLOAT, false, sizeof(float) };
}

template <>
mesh_attribute_data attr<vec2>()
{
	return { 2, gl::FLOAT, false, 2 * sizeof(float) };
}

template <>
mesh_attribute_data attr<color>()
{
	return { 4, gl::UNSIGNED_BYTE, true, 4 };
}

template <>
mesh_attribute_data attr<std::int16_t>()
{
	return { 1, gl::SHORT, false, sizeof(std::int16_t) };
}

struct mesh
{
	mesh()
	{
		gl::GenVertexArrays(1, &vao_);
		gl::GenBuffers(1, &vbo_);
	}

	~mesh()
	{
		gl::DeleteBuffers(1, &vbo_);
		gl::DeleteVertexArrays(1, &vao_);
	}

	template <typename ... Types>
	void setup(GLenum primitive_type)
	{
		primitive_type_ = primitive_type;
		std::vector<mesh_attribute_data> attr_data{attr<Types>() ...};
		setup(attr_data);
	}

	template <typename T>
	void load(std::vector<T> const & data, GLenum usage = gl::STATIC_DRAW)
	{
		if (stride_ != sizeof(T))
			fail("Wrong vertex type for mesh");

		gl::BindBuffer(gl::ARRAY_BUFFER, vbo_);
		gl::BufferData(gl::ARRAY_BUFFER, data.size() * sizeof(T), data.data(), usage);
		count_ = data.size();
	}

	void draw()
	{
		if (!primitive_type_)
			fail("Rendering mesh without prior setup");

		if (count_ != 0)
		{
			gl::BindVertexArray(vao_);
			gl::DrawArrays(*primitive_type_, 0, count_);
		}
	}

protected:
	GLuint vao_;
	GLuint vbo_;
	GLsizei count_ = 0;
	std::optional<GLenum> primitive_type_;
	GLsizei stride_ = 0;

	void setup(std::vector<mesh_attribute_data> const & attributes)
	{
		gl::BindVertexArray(vao_);
		gl::BindBuffer(gl::ARRAY_BUFFER, vbo_);
		stride_ = 0;
		for (auto const & attr : attributes) stride_ += attr.size;

		GLsizei offset = 0;
		for (GLuint index = 0; index < attributes.size(); ++index)
		{
			auto const & attr = attributes[index];

			gl::EnableVertexAttribArray(index);
			gl::VertexAttribPointer(index, attr.dimension, attr.type, attr.normalized ? gl::TRUE_ : gl::FALSE_, stride_, reinterpret_cast<char *>(offset));
			offset += attr.size;
		}
	}
};

struct indexed_mesh
	: mesh
{
	indexed_mesh()
	{
		gl::GenBuffers(1, &index_vbo_);
	}

	template <typename ... Types>
	void setup(GLenum primitive_type)
	{
		mesh::setup<Types...>(primitive_type);
		gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, index_vbo_);
	}

	template <typename T>
	void load(std::vector<T> const & data, std::vector<std::uint16_t> const & index_data, GLenum usage = gl::STATIC_DRAW)
	{
		if (stride_ != sizeof(T))
			fail("Wrong vertex type for mesh");

		using IndexType = std::uint16_t;

		if (data.size() > std::numeric_limits<IndexType>::max())
			fail("Too many vertices for this index type");

		gl::BindBuffer(gl::ARRAY_BUFFER, vbo_);
		gl::BufferData(gl::ARRAY_BUFFER, data.size() * sizeof(T), data.data(), usage);
		gl::BindBuffer(gl::ARRAY_BUFFER, index_vbo_);
		gl::BufferData(gl::ARRAY_BUFFER, index_data.size() * sizeof(IndexType), index_data.data(), usage);

		count_ = index_data.size();
		index_type_ = gl::UNSIGNED_SHORT;
	}

	void draw()
	{
		if (!primitive_type_)
			fail("Rendering mesh without prior setup");

		if (count_ != 0)
		{
			// count_ != 0 guarantees (bool)(index_type_) == true

			gl::BindVertexArray(vao_);
			gl::DrawElements(*primitive_type_, count_, *index_type_, nullptr);
		}
	}

	~indexed_mesh()
	{
		gl::DeleteBuffers(1, &index_vbo_);
	}

private:
	GLuint index_vbo_;
	std::optional<GLenum> index_type_;
};

GLuint make_shader(const char * source, GLenum type, std::string const & name)
{
	const char * sources[] = { source };

	GLuint shader = gl::CreateShader(type);
	gl::ShaderSource(shader, 1, sources, nullptr);
	gl::CompileShader(shader);

	GLint status;
	gl::GetShaderiv(shader, gl::COMPILE_STATUS, &status);
	if (status != gl::TRUE_)
	{
		int length;
		gl::GetShaderiv(shader, gl::INFO_LOG_LENGTH, &length);
		std::unique_ptr<char[]> log(new char[length + 1]);
		gl::GetShaderInfoLog(shader, length + 1, nullptr, log.get());
		fail(name, " shader compilation failed: ", log.get());
	}

	return shader;
}

GLuint make_program(const char * vertex_source, const char * fragment_source, std::string const & name)
{
	GLuint program = gl::CreateProgram();

	GLuint vertex_shader = make_shader(vertex_source, gl::VERTEX_SHADER, name + "_vert");
	at_scope_exit vertex_shader_deleter{[=]{ gl::DeleteShader(vertex_shader); }};

	GLuint fragment_shader = make_shader(fragment_source, gl::FRAGMENT_SHADER, name + "_frag");
	at_scope_exit fragment_shader_deleter{[=]{ gl::DeleteShader(fragment_shader); }};

	gl::AttachShader(program, vertex_shader);
	gl::AttachShader(program, fragment_shader);
	gl::LinkProgram(program);
	gl::DetachShader(program, vertex_shader);
	gl::DetachShader(program, fragment_shader);

	GLint status;
	gl::GetProgramiv(program, gl::LINK_STATUS, &status);
	if (status != gl::TRUE_)
	{
		int length;
		gl::GetProgramiv(program, gl::INFO_LOG_LENGTH, &length);
		std::unique_ptr<char[]> log(new char[length + 1]);
		gl::GetProgramInfoLog(program, length + 1, nullptr, log.get());
		fail(name, " program link failed: ", log.get());
	}

	return program;
}

struct program
{
	program(const char * vertex_source, const char * fragment_source, std::string const & name)
		: id_(make_program(vertex_source, fragment_source, name))
	{}

	~program()
	{
		gl::DeleteProgram(id_);
	}

	void use()
	{
		gl::UseProgram(id_);
	}

	template <typename T>
	void uniform(std::string const & name, T const & value)
	{
		if (uniform_location_.count(name) == 0)
		{
			uniform_location_[name] = gl::GetUniformLocation(id_, name.data());
		}

		uniform_impl(uniform_location_[name], value);
	}

private:
	GLuint id_;

	std::unordered_map<std::string, GLint> uniform_location_;

	void uniform_impl(GLint location, int value)
	{
		gl::Uniform1i(location, value);
	}

	void uniform_impl(GLint location, float value)
	{
		gl::Uniform1f(location, value);
	}

	void uniform_impl(GLint location, vec2 const & value)
	{
		gl::Uniform2f(location, value.x, value.y);
	}

	void uniform_impl(GLint location, color const & col)
	{
		gl::Uniform4f(location, col.r / 255.f, col.g / 255.f, col.b / 255.f, col.a / 255.f);
	}
};

const char stars_vertex_glsl[] =
R"(#version 330 core

uniform vec2 camera_pos;
uniform float camera_scale;
uniform float aspect_ratio;
uniform vec2 offset;

layout (location = 0) in vec2 in_position;
layout (location = 1) in float in_depth;
layout (location = 2) in float in_intensity;

out float intensity;

void main()
{
	gl_Position = vec4((in_position + offset - camera_pos) / in_depth / vec2(camera_scale * aspect_ratio, camera_scale), 0.0, 1.0);
	intensity = in_intensity;
}
)";

const char stars_fragment_glsl[] =
R"(#version 330 core

in float intensity;

out vec4 out_color;

void main()
{
	out_color = vec4(intensity, intensity, intensity, 1.0);
}
)";

const char ship_vertex_glsl[] =
R"(#version 330 core

uniform vec2 camera_pos;
uniform float camera_scale;
uniform float aspect_ratio;
uniform vec2 offset;
uniform float angle;

uniform vec4 color_multiplier;

layout (location = 0) in vec2 in_position;
layout (location = 1) in vec4 in_color;
layout (location = 2) in float in_depth;

out vec4 color;

void main()
{
	float c = cos(angle);
	float s = sin(angle);

	vec2 rotated = vec2(c * in_position.x - s * in_position.y, s * in_position.x + c * in_position.y);

	gl_Position = vec4((offset + rotated - camera_pos) / vec2(camera_scale * aspect_ratio, camera_scale), 0.0, 1.0);
	color = vec4(mix(in_color.rgb, color_multiplier.rgb, 1.0 - color_multiplier.a), in_color.a * color_multiplier.a);
}
)";

const char ship_fragment_glsl[] =
R"(#version 330 core

in vec4 color;

out vec4 out_color;

void main()
{
	out_color = color;
}
)";

const char colored_world_vertex_glsl[] =
R"(#version 330 core

uniform vec2 camera_pos;
uniform float camera_scale;
uniform float aspect_ratio;

layout (location = 0) in vec2 in_position;
layout (location = 1) in vec4 in_color;

out vec4 color;

void main()
{
	gl_Position = vec4((in_position - camera_pos) / vec2(camera_scale * aspect_ratio, camera_scale), 0.0, 1.0);
	color = in_color;
}
)";

const char colored_world_fragment_glsl[] =
R"(#version 330 core

in vec4 color;

out vec4 out_color;

void main()
{
	out_color = color;
}
)";

const char colored_screen_vertex_glsl[] =
R"(#version 330 core

layout (location = 0) in vec2 in_position;
layout (location = 1) in vec4 in_color;

out vec4 color;

void main()
{
	gl_Position = vec4(in_position, 0.0, 1.0);
	color = in_color;
}
)";

const char colored_screen_fragment_glsl[] =
R"(#version 330 core

in vec4 color;

out vec4 out_color;

void main()
{
	out_color = color;
}
)";

const char textured_screen_vertex_glsl[] =
R"(#version 330 core

layout (location = 0) in vec2 in_position;
layout (location = 1) in vec2 in_texcoord;

out vec2 texcoord;

void main()
{
	gl_Position = vec4(in_position, 0.0, 1.0);
	texcoord = in_texcoord;
}
)";

const char textured_screen_fragment_glsl[] =
R"(#version 330 core

uniform sampler2D tex;

in vec2 texcoord;

out vec4 out_color;

void main()
{
	out_color = texture(tex, texcoord);
}
)";

const char gaussian_blur_horizontal_vertex_glsl[] =
R"(#version 330 core

uniform int in_width;

layout (location = 0) in vec2 in_position;
layout (location = 1) in vec2 in_texcoord;

const int N = 4;

out vec2 texcoord[2 * N + 1];

void main()
{
	gl_Position = vec4(in_position, 0.0, 1.0);

	for (int i = 0; i < 2 * N + 1; ++i)
	{
		texcoord[i] = in_texcoord + vec2(float(i - N) / float(in_width), 0.0);
	}
}
)";

const char gaussian_blur_vertical_vertex_glsl[] =
R"(#version 330 core

uniform int in_height;

layout (location = 0) in vec2 in_position;
layout (location = 1) in vec2 in_texcoord;

const int N = 4;

out vec2 texcoord[2 * N + 1];

void main()
{
	gl_Position = vec4(in_position, 0.0, 1.0);

	for (int i = 0; i < 2 * N + 1; ++i)
	{
		texcoord[i] = in_texcoord + vec2(0.0, float(i - N) / float(in_height));
	}
}
)";

const char gaussian_blur_fragment_glsl[] =
R"(#version 330 core

uniform sampler2D in_texture;

in vec2 texcoord[9];

out vec4 out_color;

void main()
{
	const int N = 4;

	vec4 res = vec4(0.0, 0.0, 0.0, 0.0);

//	float c[2 * N + 1] = float[2 * N + 1](0.063327, 0.093095, 0.122589, 0.144599, 0.152781, 0.144599, 0.122589, 0.093095, 0.063327); // sigma = 3

	float c[2 * N + 1] = float[2 * N + 1](0.028532, 0.067234, 0.124009, 0.179044, 0.20236, 0.179044, 0.124009, 0.067234, 0.028532); // sigma = 2

	for (int i = 0; i < 2 * N + 1; ++i)
	{
		vec4 p = texture(in_texture, texcoord[i]);
		res += vec4(p.rgb * p.a, p.a) * c[i];
	}

	if (res.a == 0)
		out_color = vec4(0.0, 0.0, 0.0, 0.0);
	else
		out_color = vec4(res.rgb / res.a, res.a);
}
)";

struct texture
{
	texture();

	~texture();

	GLuint id() const { return id_; }

	void bind();

	void load(int width, int height, void const * data = nullptr);

private:
	GLuint id_;
};

struct framebuffer
{
	framebuffer();

	~framebuffer();

	GLuint id() const { return id_; }

	void bind();

	void attach(texture const & t);

	void assert_complete();

	static void bind_default();

private:
	GLuint id_;
};

framebuffer::framebuffer()
{
	gl::GenFramebuffers(1, &id_);
}

framebuffer::~framebuffer()
{
	gl::DeleteFramebuffers(1, &id_);
}

void framebuffer::bind()
{
	gl::BindFramebuffer(gl::DRAW_FRAMEBUFFER, id_);
}

void framebuffer::attach(texture const & t)
{
	bind();
	gl::FramebufferTexture2D(gl::DRAW_FRAMEBUFFER, gl::COLOR_ATTACHMENT0, gl::TEXTURE_2D, t.id(), 0);
}

void framebuffer::assert_complete()
{
	bind();
	auto status = gl::CheckFramebufferStatus(gl::DRAW_FRAMEBUFFER);
	if (status != gl::FRAMEBUFFER_COMPLETE)
		throw std::runtime_error("framebuffer not complete");
}

void framebuffer::bind_default()
{
	gl::BindFramebuffer(gl::DRAW_FRAMEBUFFER, 0);
}

struct post_processing_effect
{
	post_processing_effect(char const * vertex_source, char const * fragment_source, std::string name);

	void resize(int width, int height);

	int width() const { return width_; }
	int height() const { return height_; }

	void bind();
	texture & out_texture() { return out_texture_; }

	void execute(texture & in_texture, int width, int height);

private:
	program program_;
	framebuffer framebuffer_;
	texture out_texture_;
	int width_ = 0;
	int height_ = 0;

	mesh mesh_;
};

post_processing_effect::post_processing_effect(char const * vertex_source, char const * fragment_source, std::string name)
	: program_{vertex_source, fragment_source, std::move(name)}
{
	mesh_.setup<vec2, vec2>(gl::TRIANGLES);

	struct vertex
	{
		vec2 pos;
		vec2 texcoord;
	};

	std::vector<vertex> vertices
	{
		{{-1.f, -1.f}, {0.f, 0.f}},
		{{ 1.f, -1.f}, {1.f, 0.f}},
		{{ 1.f,  1.f}, {1.f, 1.f}},
		{{-1.f, -1.f}, {0.f, 0.f}},
		{{ 1.f,  1.f}, {1.f, 1.f}},
		{{-1.f,  1.f}, {0.f, 1.f}},
	};

	mesh_.load(vertices);
}

void post_processing_effect::resize(int width, int height)
{
	out_texture_.load(width, height);
	framebuffer_.attach(out_texture_);
	framebuffer_.assert_complete();

	width_ = width;
	height_ = height;
}

void post_processing_effect::bind()
{
	framebuffer_.bind();
	gl::Viewport(0, 0, width_, height_);
}

void post_processing_effect::execute(texture & in_texture, int width, int height)
{
	bind();

	gl::Disable(gl::BLEND);

	program_.use();
	program_.uniform("in_texture", 0);
	program_.uniform("in_width", width);
	program_.uniform("in_height", height);
	in_texture.bind();

	mesh_.draw();

	gl::Enable(gl::BLEND);
}

texture::texture()
{
	gl::GenTextures(1, &id_);

	bind();
	gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::NEAREST);
	gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR);
	gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, gl::CLAMP_TO_EDGE);
	gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, gl::CLAMP_TO_EDGE);
}

texture::~texture()
{
	gl::DeleteTextures(1, &id_);
}

void texture::load(int width, int height, void const * data)
{
	bind();
	gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RGBA8, width, height, 0, gl::RGBA, gl::UNSIGNED_BYTE, data);
}

void texture::bind()
{
	gl::BindTexture(gl::TEXTURE_2D, id_);
}

struct stats
{
	int level = 1;
	float distance = 0.f;
	int bullets = 0;
	int cannons = 0;
	int rockets = 0;
	int time_played = 0;
	float kill_efficiency = 0.f;

	void reset()
	{
		level = 1;
		distance = 0.f;
		bullets = 0;
		cannons = 0;
		rockets = 0;
		time_played = 0;
		kill_efficiency = 0.f;
	}

	void read();
	void write();

	static std::string filename();
};

std::string stats::filename()
{
	return "records.txt";
}

void stats::read()
{
	std::ifstream is{filename()};
	if (!is) return;

	is >> level >> distance >> bullets >> cannons >> rockets >> time_played >> kill_efficiency;
}

void stats::write()
{
	std::ofstream os{filename()};
	if (!os)
	{
		std::cerr << "Failed to open records file for writing!" << std::endl;
		return;
	}

	os
		<< level << '\n'
		<< distance << '\n'
		<< bullets << '\n'
		<< cannons << '\n'
		<< rockets << '\n'
		<< time_played << '\n'
		<< kill_efficiency << '\n'
		;
}

struct text
{
	text();

	void render(float aspect_ratio);

	void add(std::string text, vec2 origin, int x_align, int y_align, color col);
	void reset();

private:
	program program_;
	mesh mesh_;

	std::vector<std::vector<vec2>> letters_;

	struct text_rec
	{
		std::string text;
		vec2 origin;
		int x_align;
		int y_align;
		color col;
	};

	std::vector<text_rec> texts_;
};

text::text()
	: program_{colored_screen_vertex_glsl, colored_screen_fragment_glsl, "text"}
{
	mesh_.setup<vec2, color>(gl::LINES);

	letters_.resize(128);

	// next-gen text rendering technique

	letters_['A'] = {{0.f, 0.f}, {0.f, 1.f}, {1.f, 0.f}, {1.f, 1.f}, {0.f, 1.f}, {1.f, 1.f}, {0.f, 0.5f}, {1.f, 0.5f}};
	letters_['B'] = {{0.f, 0.f}, {0.f, 1.f}, {0.f, 0.f}, {0.5f, 0.f}, {0.f, 0.5f}, {0.5f, 0.5f}, {0.f, 1.f}, {0.5f, 1.f}, {0.5f, 0.f}, {1.f, 0.25f}, {1.f, 0.25f}, {0.5f, 0.5f}, {0.5f, 0.5f}, {1.f, 0.75f}, {1.f, 0.75f}, {0.5f, 1.f}};
	letters_['C'] = {{1.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 1.f}, {0.f, 1.f}, {1.f, 1.f}};
	letters_['D'] = {{0.f, 0.f}, {0.f, 1.f}, {0.f, 0.f}, {0.5f, 0.f}, {0.f, 1.f}, {0.5f, 1.f}, {0.5f, 0.f}, {1.f, 0.25f}, {0.5f, 1.f}, {1.f, 0.75f}, {1.f, 0.25f}, {1.f, 0.75f}};
	letters_['E'] = {{1.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 1.f}, {0.f, 1.f}, {1.f, 1.f}, {0.f, 0.5f}, {1.f, 0.5f}};
	letters_['F'] = {{0.f, 0.f}, {0.f, 1.f}, {0.f, 1.f}, {1.f, 1.f}, {0.f, 0.5f}, {1.f, 0.5f}};
	letters_['G'] = {{0.f, 0.f}, {0.f, 1.f}, {0.f, 1.f}, {1.f, 1.f}, {0.f, 0.f}, {0.5f, 0.f}, {0.5f, 0.f}, {1.f, 0.25f}, {1.f, 0.f}, {1.f, 0.5f}};
	letters_['H'] = {{0.f, 0.f}, {0.f, 1.f}, {1.f, 0.f}, {1.f, 1.f}, {0.f, 0.5f}, {1.f, 0.5f}};
	letters_['I'] = {{0.f, 0.f}, {1.f, 0.f}, {0.f, 1.f}, {1.f, 1.f}, {0.5f, 0.f}, {0.5f, 1.f}};
	letters_['J'] = {{0.f, 1.f}, {1.f, 1.f}, {1.f, 1.f}, {1.f, 0.25f}, {1.f, 0.25f}, {0.5f, 0.f}, {0.5f, 0.f}, {0.f, 0.25f}};
	letters_['K'] = {{0.f, 0.f}, {0.f, 1.f}, {0.f, 0.5f}, {1.f, 0.f}, {0.f, 0.5f}, {1.f, 1.f}};
	letters_['L'] = {{0.f, 0.f}, {0.f, 1.f}, {0.f, 0.f}, {1.f, 0.f}};
	letters_['M'] = {{0.f, 0.f}, {0.f, 1.f}, {0.f, 1.f}, {0.5f, 0.5f}, {0.5f, 0.5f}, {1.f, 1.f}, {1.f, 1.f}, {1.f, 0.f}};
	letters_['N'] = {{0.f, 0.f}, {0.f, 1.f}, {0.f, 1.f}, {1.f, 0.f}, {1.f, 0.f}, {1.f, 1.f}};
	letters_['O'] = {{0.f, 0.f}, {0.f, 1.f}, {0.f, 1.f}, {1.f, 1.f}, {1.f, 1.f}, {1.f, 0.f}, {1.f, 0.f}, {0.f, 0.f}};
	letters_['P'] = {{0.f, 0.f}, {0.f, 1.f}, {0.f, 1.f}, {1.f, 1.f}, {1.f, 1.f}, {1.f, 0.5f}, {1.f, 0.5f}, {0.f, 0.5f}};
	letters_['Q'] = {{0.f, 0.f}, {0.f, 1.f}, {0.f, 1.f}, {1.f, 1.f}, {1.f, 1.f}, {1.f, 0.25f}, {1.f, 0.25f}, {0.5f, 0.f}, {0.5f, 0.f}, {0.f, 0.f}, {0.5f, 0.25f}, {1.f, 0.f}};
	letters_['R'] = {{0.f, 0.f}, {0.f, 1.f}, {0.f, 1.f}, {1.f, 1.f}, {1.f, 1.f}, {1.f, 0.5f}, {1.f, 0.5f}, {0.f, 0.5f}, {0.f, 0.5f}, {1.f, 0.f}};
	letters_['S'] = {{0.f, 0.f}, {1.f, 0.f}, {1.f, 0.f}, {1.f, 0.5f}, {1.f, 0.5f}, {0.f, 0.5f}, {0.f, 0.5f}, {0.f, 1.f}, {0.f, 1.f}, {1.f, 1.f}};
	letters_['T'] = {{0.5f, 0.f}, {0.5f, 1.f}, {0.f, 1.f}, {1.f, 1.f}};
	letters_['U'] = {{0.f, 1.f}, {0.f, 1.f/6}, {0.f, 1.f/6}, {1.f/3, 0.f}, {1.f/3, 0.f}, {2.f/3, 0.f}, {2.f/3, 0.f}, {1.f, 1.f/6}, {1.f, 1.f/6}, {1.f, 1.f}};
	letters_['V'] = {{0.f, 1.f}, {0.5f, 0.f}, {0.5f, 0.f}, {1.f, 1.f}};
	letters_['W'] = {{0.f, 1.f}, {0.25f, 0.f}, {0.25f, 0.f}, {0.5f, 0.5f}, {0.5f, 0.5f}, {0.75f, 0.f}, {0.75f, 0.f}, {1.f, 1.f}};
	letters_['X'] = {{0.f, 0.f}, {1.f, 1.f}, {0.f, 1.f}, {1.f, 0.f}};
	letters_['Y'] = {{0.f, 1.f}, {0.5f, 0.5f}, {1.f, 1.f}, {0.5f, 0.5f}, {0.5f, 0.f}, {0.5f, 0.5f}};
	letters_['Z'] = {{0.f, 1.f}, {1.f, 1.f}, {1.f, 1.f}, {0.f, 0.f}, {0.f, 0.f}, {1.f, 0.f}, {0.25f, 0.5f}, {0.75f, 0.5f}};

	letters_['0'] = {{0.f, 0.f}, {0.f, 1.f}, {0.f, 1.f}, {1.f, 1.f}, {1.f, 1.f}, {1.f, 0.f}, {1.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {1.f, 1.f}};
	letters_['1'] = {{0.f, 0.f}, {1.f, 0.f}, {0.5f, 0.f}, {0.5f, 1.f}, {0.5f, 1.f}, {0.f, 0.75f}};
	letters_['2'] = {{1.f, 0.f}, {0.f, 0.f}, {0.f, 0.f}, {0.f, 0.5f}, {0.f, 0.5f}, {1.f, 0.5f}, {1.f, 0.5f}, {1.f, 1.f}, {1.f, 1.f}, {0.f, 1.f}};
	letters_['3'] = {{0.f, 0.f}, {1.f, 0.f}, {1.f, 0.f}, {1.f, 1.f}, {1.f, 1.f}, {0.f, 1.f}, {1.f, 0.5f}, {0.f, 0.5f}};
	letters_['4'] = {{0.f, 1.f}, {0.f, 0.5f}, {0.f, 0.5f}, {1.f, 0.5f}, {1.f, 0.f}, {1.f, 1.f}};
	letters_['5'] = {{0.f, 0.f}, {1.f, 0.f}, {1.f, 0.f}, {1.f, 0.5f}, {1.f, 0.5f}, {0.f, 0.5f}, {0.f, 0.5f}, {0.f, 1.f}, {0.f, 1.f}, {1.f, 1.f}};
	letters_['6'] = {{0.f, 0.f}, {1.f, 0.f}, {1.f, 0.f}, {1.f, 0.5f}, {1.f, 0.5f}, {0.f, 0.5f}, {0.f, 0.f}, {0.f, 1.f}, {0.f, 1.f}, {1.f, 1.f}};
	letters_['7'] = {{0.f, 1.f}, {1.f, 1.f}, {1.f, 1.f}, {1.f, 0.f}};
	letters_['8'] = {{0.f, 0.f}, {0.f, 1.f}, {0.f, 1.f}, {1.f, 1.f}, {1.f, 1.f}, {1.f, 0.f}, {1.f, 0.f}, {0.f, 0.f}, {0.f, 0.5f}, {1.f, 0.5f}};
	letters_['9'] = {{1.f, 1.f}, {0.f, 1.f}, {0.f, 1.f}, {0.f, 0.5f}, {0.f, 0.5f}, {1.f, 0.5f}, {1.f, 1.f}, {1.f, 0.f}, {1.f, 0.f}, {0.f, 0.f}};

	letters_['-'] = {{0.f, 0.5f}, {1.f, 0.5f}};
	letters_[':'] = {{0.5f, 1.f/6}, {0.5f, 2.f/6}, {0.5f, 4.f/6}, {0.5f, 5.f/6}};
	letters_['?'] = {{0.f, 0.75f}, {0.5f, 1.f}, {0.5f, 1.f}, {1.f, 0.75f}, {1.f, 0.75f}, {0.5f, 0.5f}, {0.5f, 0.5f}, {0.5f, 0.25f}, {0.5f, 0.1f}, {0.5f, 0.f}};
	letters_['!'] = {{0.5f, 1.f}, {0.5f, 0.3f}, {0.5f, 0.1f}, {0.5f, 0.f}};
	letters_['.'] = {{0.5f, 0.1f}, {0.5f, 0.f}};
	letters_['%'] = {{0.f, 1.f}, {0.5f, 1.f}, {0.5f, 1.f}, {0.5f, 0.75f}, {0.5f, 0.75f}, {0.f, 0.75f}, {0.f, 0.75f}, {0.f, 1.f}, {1.f, 0.f}, {0.5f, 0.f}, {0.5f, 0.f}, {0.5f, 0.25f}, {0.5f, 0.25f}, {1.f, 0.25f}, {1.f, 0.25f}, {1.f, 0.f}, {0.f, 0.f}, {1.f, 1.f}};
	letters_['['] = {{0.25f, 0.f}, {0.75f, 0.f}, {0.25f, 0.f}, {0.25f, 1.f}, {0.25f, 1.f}, {0.75f, 1.f}};
	letters_[']'] = {{0.75f, 0.f}, {0.25f, 0.f}, {0.75f, 0.f}, {0.75f, 1.f}, {0.75f, 1.f}, {0.25f, 1.f}};
	letters_['/'] = {{0.f, 0.f}, {1.f, 1.f}};
	letters_['\''] = {{0.5f, 0.8f}, {0.5f, 1.f}};
}

void text::render(float aspect_ratio)
{
	struct vertex
	{
		vec2 pos;
		color col;
	};

	std::vector<vertex> vertices;

	float const H = 0.05f;
	float const W = H * 0.5f / aspect_ratio;
	float const spacing = W * 0.5f;

	for (auto const & rec : texts_)
	{
		if (rec.text.empty()) continue;

		float width = (rec.text.size() - 1) * (W + spacing) + W;
		float height = H;

		vec2 o = rec.origin;
		o.x -= (rec.x_align + 1) * 0.5f * width;
		o.y -= (rec.y_align + 1) * 0.5f * height;

		for (char c : rec.text)
		{
			if (c == ' ')
			{
				o.x += W + spacing;
				continue;
			}

			if (c >= 128) c = '?';
			if (letters_[c].empty()) c = '?';

			for (int i = 0; i < letters_[c].size(); ++i)
			{
				vertices.push_back({{o.x + letters_[c][i].x * W, o.y + letters_[c][i].y * H}, rec.col});
			}

			o.x += W + spacing;
		}
	}

	mesh_.load(vertices, gl::STREAM_DRAW);

	program_.use();
	mesh_.draw();
}

void text::add(std::string text, vec2 origin, int x_align, int y_align, color col)
{
	texts_.push_back({std::move(text), origin, x_align, y_align, col});
}

void text::reset()
{
	texts_.clear();
}

struct stars
{
	stars(random_engine & rng);

	void render(vec2 const & camera_pos, float camera_scale, float aspect_ratio);

private:
	program program_;

	mesh mesh_;

	float const max_depth = 10.f;
	float const batch_size = 1000.f;
};

stars::stars(random_engine & rng)
	: program_{ stars_vertex_glsl, stars_fragment_glsl, "stars" }
{
	mesh_.setup<vec2, float, float>(gl::POINTS);

	struct star
	{
		vec2 position;
		float depth;
		float intensity;
	};

	std::vector<star> stars;
	for (int i = 0; i < 1000; ++i)
	{
		vec2 const pos { rng(0.f, batch_size), rng(0.f, batch_size) };
		float const depth = rng(1.f, max_depth);

		stars.push_back({pos, depth, rng() });
	}

	mesh_.load(stars);
}

void stars::render(vec2 const & camera_pos, float camera_scale, float aspect_ratio)
{
	program_.use();
	program_.uniform("camera_pos", camera_pos);
	program_.uniform("camera_scale", camera_scale);
	program_.uniform("aspect_ratio", aspect_ratio);

	for (int cx = std::floor((camera_pos.x - camera_scale * aspect_ratio * max_depth) / batch_size);
		cx <= std::ceil((camera_pos.x + camera_scale * aspect_ratio * max_depth) / batch_size); ++cx)
	{
		for (int cy = std::floor((camera_pos.y - camera_scale * max_depth) / batch_size);
			cy <= std::ceil((camera_pos.y + camera_scale * max_depth) / batch_size); ++cy)
		{
			program_.uniform("offset", vec2{cx * batch_size, cy * batch_size});
			mesh_.draw();
		}
	}
}

struct ship
{
	vec2 pos;
	vec2 vel;
	float angle;
	float angle_tgt;

	float fire_reload;

	float experience;

	float propulsion;
	bool show_history;

	float health;

	ship(random_engine & rng);
	void render(vec2 const & camera_pos, float camera_scale, float aspect_ratio);
	void render_glow(vec2 const & camera_pos, float camera_scale, float aspect_ratio);

	void reset()
	{
		pos = {0.f, 0.f};
		vel = {0.f, 0.f};
		angle = pi / 2.f;
		angle_tgt = angle;
		fire_reload = 0.f;
		experience = 1.f;
		propulsion = 0.f;
		show_history = false;
		health = 1.f;
		history_dt_ = 0.f;
		history_.clear();
	}

	void update(float dt, float boost);
	float rewind_status() const;
	bool can_rewind() const;
	void rewind();
	vec2 rewind_pos() const;
	void abort_rewind()
	{
		history_.clear();
	}

private:
	random_engine & rng_;
	program program_;
	mesh mesh_;
	mesh propulsion_mesh_;

	struct snapshot
	{
		vec2 pos;
		vec2 vel;
		float angle;
		float health;
	};

	static constexpr int history_max = 200;
	std::deque<snapshot> history_;
	float history_dt_ = 0.f;
};

ship::ship(random_engine & rng)
	: rng_(rng)
	, program_{ ship_vertex_glsl, ship_fragment_glsl, "ship" }
{
	reset();

	mesh_.setup<vec2, color, float>(gl::TRIANGLES);
	propulsion_mesh_.setup<vec2, color, float>(gl::TRIANGLES);

	struct vertex
	{
		vec2 position;
		color col;
		float depth;
	};

	std::vector<vertex> vertices;

	vertices.push_back({{0.f, 0.f}, {0, 0, 255, 255}, 0.f});
	vertices.push_back({{2.5f, 0.f}, {0, 0, 255, 255}, 0.f});
	vertices.push_back({{-2.5f, -1.5f}, {255, 255, 255, 255}, 0.f});
	vertices.push_back({{0.f, 0.f}, {0, 0, 255, 255}, 0.f});
	vertices.push_back({{2.5f, 0.f}, {0, 0, 255, 255}, 0.f});
	vertices.push_back({{-2.5f,  1.5f}, {255, 255, 255, 255}, 0.f});

	mesh_.load(vertices);
}

void ship::render(vec2 const & camera_pos, float camera_scale, float aspect_ratio)
{
	program_.use();
	program_.uniform("camera_pos", camera_pos);
	program_.uniform("camera_scale", camera_scale);
	program_.uniform("aspect_ratio", aspect_ratio);
	program_.uniform("offset", pos);
	program_.uniform("angle", angle);
	program_.uniform("color_multiplier", color{255, 255, 255, 255});
	{
		struct vertex
		{
			vec2 position;
			color col;
			float depth;
		};

		std::vector<vertex> vertices;

		for (int i = 0; i < 10; ++i)
		{
			float p = 0.5f + 0.5f * propulsion;
			float r = 0.6f;

			vertices.push_back({vec2{-3.f + rng_(-r, r), rng_(-r, r)} * p, {255, 0, 255, 127}, 0.f});
			vertices.push_back({{0.f, 0.5f}, {0, 255, 255, 0}, 0.f});
			vertices.push_back({{0.f, -0.5f}, {0, 255, 255, 0}, 0.f});
		}

		propulsion_mesh_.load(vertices);
		propulsion_mesh_.draw();
	}
	mesh_.draw();

	if (can_rewind() && show_history)
	{
		vec2 pos = history_.front().pos;
		float angle = history_.front().angle;

		if (history_.size() >= 2)
		{
			float t = history_dt_ / 0.01f;

			pos = lerp(pos, history_[1].pos, t);
			angle = lerp(angle, history_[1].angle, t);
		}

		program_.uniform("offset", pos);
		program_.uniform("angle", angle);
		program_.uniform("color_multiplier", color{127, 255, 255, 95});
		mesh_.draw();
	}
}

void ship::render_glow(vec2 const & camera_pos, float camera_scale, float aspect_ratio)
{
	program_.use();
	program_.uniform("camera_pos", camera_pos);
	program_.uniform("camera_scale", camera_scale);
	program_.uniform("aspect_ratio", aspect_ratio);
	program_.uniform("offset", pos);
	program_.uniform("angle", angle);
	program_.uniform("color_multiplier", color{255, 255, 255, 255});
	{
		struct vertex
		{
			vec2 position;
			color col;
			float depth;
		};

		std::vector<vertex> vertices;

		for (int i = 0; i < 10; ++i)
		{
			float p = 0.5f + 0.5f * propulsion;
			float r = 0.6f;

			vertices.push_back({vec2{-3.f + rng_(-r, r), rng_(-r, r)} * p, {255, 0, 255, 127}, 0.f});
			vertices.push_back({{0.f, 0.5f}, {0, 255, 255, 0}, 0.f});
			vertices.push_back({{0.f, -0.5f}, {0, 255, 255, 0}, 0.f});
		}

		propulsion_mesh_.load(vertices);
		propulsion_mesh_.draw();
	}

	if (can_rewind() && show_history)
	{
		vec2 pos = history_.front().pos;
		float angle = history_.front().angle;

		if (history_.size() >= 2)
		{
			float t = history_dt_ / 0.01f;

			pos = lerp(pos, history_[1].pos, t);
			angle = lerp(angle, history_[1].angle, t);
		}

		program_.uniform("offset", pos);
		program_.uniform("angle", angle);
		program_.uniform("color_multiplier", color{127, 255, 255, 95});
		mesh_.draw();
	}
}

void ship::update(float dt, float boost)
{
	history_dt_ += dt;
	while (history_dt_ > 0.01f)
	{
		history_dt_ -= 0.01f;
		history_.push_back({pos, vel, angle, health});
	}
	while (history_.size() > history_max)
		history_.pop_front();

	if (fire_reload > 0.f)
		fire_reload -= dt;

	if (boost != 0.f)
		propulsion += 10.f * dt;
	else
		propulsion -= 20.f * dt;

	if (propulsion < 0.f) propulsion = 0.f;
	if (propulsion > 1.f) propulsion = 1.f;

	vel += boost * dir(angle) * dt;
	pos += vel * dt;

	angle += (angle_tgt - angle) * 20.f * dt;

	health += 0.02f * dt;
	if (health > 1.f) health = 1.f;
}

float ship::rewind_status() const
{
	return static_cast<float>(history_.size()) / history_max;
}

bool ship::can_rewind() const
{
	return history_.size() == history_max;
}

void ship::rewind()
{
	pos = history_.front().pos;
	vel = history_.front().vel;
	angle = history_.front().angle;
	angle_tgt = angle;
	health = history_.front().health;
	history_.clear();
}

vec2 ship::rewind_pos() const
{
	return history_.front().pos;
}

struct terrain
{
	static constexpr float threshold = 0.5f;

	terrain(random_engine & rng)
		: rng_(rng)
		, program_(colored_world_vertex_glsl, colored_world_fragment_glsl, "terrain")
	{}

	void render(vec2 const & camera_pos, float camera_scale, float aspect_ratio);

	float value(vec2 p) const;
	vec2 gradient(vec2 p)  const;

	void explode(vec2 p, float radius, float strength);

	void reset()
	{
		cells_.clear();
	}

private:

	random_engine & rng_;

	program program_;

	struct cell_id
	{
		int x, y;
	};

	static constexpr int tile_size = 4;
	static constexpr int tiles_per_noise = 8;
	static constexpr int noise_per_cell = 4;
	static constexpr int tiles_per_cell = tiles_per_noise * noise_per_cell;
	static constexpr int cell_size = tile_size * tiles_per_cell;

	struct cell
	{
		std::array<float, (noise_per_cell + 1) * (noise_per_cell + 1)> noise_values;
		std::array<float, (tiles_per_cell + 1) * (tiles_per_cell + 1)> values;

		bool mesh_needs_update = true;
		std::optional<::mesh> mesh;
	};

	struct cell_id_hash
	{
		std::hash<int> h;

		std::size_t operator() (cell_id const & id) const
		{
			std::size_t seed = 0;
			hash_combine(seed, h(id.x));
			hash_combine(seed, h(id.y));
			return seed;
		}
	};

	struct cell_id_equal
	{
		bool operator() (cell_id const & id1, cell_id const & id2) const
		{
			return (id1.x == id2.x) && (id1.y == id2.y);
		}
	};

	mutable std::unordered_map<cell_id, cell, cell_id_hash, cell_id_equal> cells_;

	cell & get_cell(cell_id const & id)  const;
};

void terrain::render(vec2 const & camera_pos, float camera_scale, float aspect_ratio)
{
	program_.use();
	program_.uniform("camera_pos", camera_pos);
	program_.uniform("camera_scale", camera_scale);
	program_.uniform("aspect_ratio", aspect_ratio);

	for (int x = std::floor((camera_pos.x - camera_scale * aspect_ratio) / cell_size);
		x <= std::ceil((camera_pos.x + camera_scale * aspect_ratio) / cell_size); ++x)
	{
		for (int y = std::floor((camera_pos.y - camera_scale) / cell_size);
			y <= std::ceil((camera_pos.y + camera_scale) / cell_size); ++y)
		{
			get_cell({x, y}).mesh->draw();
		}
	}
}

float terrain::value(vec2 p) const
{
	cell_id id;
	id.x = std::floor(p.x / cell_size);
	id.y = std::floor(p.y / cell_size);

	cell const & c = get_cell(id);

	p.x -= id.x * cell_size;
	p.y -= id.y * cell_size;

	int x = std::floor(p.x / tile_size);
	int y = std::floor(p.y / tile_size);

	x = std::max(x, 0);
	y = std::max(y, 0);
	x = std::min(x, tiles_per_cell - 1);
	y = std::min(y, tiles_per_cell - 1);

	p.x -= x * tile_size;
	p.y -= y * tile_size;

	float tx = p.x / tile_size;
	float ty = p.y / tile_size;

	float vx0 = lerp(c.values[x + y * (tiles_per_cell + 1)], c.values[x + 1 + y * (tiles_per_cell + 1)], tx);
	float vx1 = lerp(c.values[x + (y + 1) * (tiles_per_cell + 1)], c.values[x + 1 + (y + 1) * (tiles_per_cell + 1)], tx);

	float v = lerp(vx0, vx1, ty);

	return v;
}

vec2 terrain::gradient(vec2 p) const
{
	cell_id id;
	id.x = std::floor(p.x / cell_size);
	id.y = std::floor(p.y / cell_size);

	cell const & c = get_cell(id);

	p.x -= id.x * cell_size;
	p.y -= id.y * cell_size;

	int x = std::floor(p.x / tile_size);
	int y = std::floor(p.y / tile_size);

	x = std::max(x, 0);
	y = std::max(y, 0);
	x = std::min(x, tiles_per_cell - 1);
	y = std::min(y, tiles_per_cell - 1);

	p.x -= x * tile_size;
	p.y -= y * tile_size;

	float tx = p.x / tile_size;
	float ty = p.y / tile_size;

	float v00 = c.values[x + y * (tiles_per_cell + 1)];
	float v10 = c.values[x + 1 + y * (tiles_per_cell + 1)];
	float v01 = c.values[x + (y + 1) * (tiles_per_cell + 1)];
	float v11 = c.values[x + 1 + (y + 1) * (tiles_per_cell + 1)];

	vec2 g;
	g.x = lerp(v10 - v00, v11 - v01, ty);
	g.y = lerp(v01 - v00, v11 - v10, tx);
	return g;
}

void terrain::explode(vec2 p, float radius, float strength)
{
	for (int cx = std::floor((p.x - radius) / cell_size); cx <= std::ceil((p.x + radius) / cell_size); ++cx)
	{
		for (int cy = std::floor((p.y - radius) / cell_size); cy <= std::ceil((p.y + radius) / cell_size); ++cy)
		{
			auto it = cells_.find({cx, cy});
			if (it == cells_.end()) continue;

			cell & c = it->second;
			c.mesh_needs_update = true;

			for (int x = 0; x <= tiles_per_cell; ++x)
			{
				for (int y = 0; y <= tiles_per_cell; ++y)
				{
					vec2 q;
					q.x = cx * cell_size + x * tile_size;
					q.y = cy * cell_size + y * tile_size;

					float r = distance(q, p);
					if (r <= radius)
					{
						float & v = c.values[x + y * (tiles_per_cell + 1)];

						v -= strength * (1.f - r / radius);
						v = std::max(v, 0.f);
					}
				}
			}
		}
	}
}

terrain::cell & terrain::get_cell(cell_id const & id) const
{
	auto [ it, new_cell ] = cells_.emplace(std::piecewise_construct, std::forward_as_tuple(id), std::forward_as_tuple());

	cell & c = it->second;

	if (new_cell)
	{
		for (auto & v : c.noise_values)
			v = rng_();

		if (auto n = cells_.find({id.x - 1, id.y}); n != cells_.end())
		{
			for (int y = 0; y <= noise_per_cell; ++y)
			{
				c.noise_values[y * (noise_per_cell + 1)] = n->second.noise_values[noise_per_cell + y * (noise_per_cell + 1)];
			}
		}

		if (auto n = cells_.find({id.x + 1, id.y}); n != cells_.end())
		{
			for (int y = 0; y <= noise_per_cell; ++y)
			{
				c.noise_values[noise_per_cell + y * (noise_per_cell + 1)] = n->second.noise_values[y * (noise_per_cell + 1)];
			}
		}

		if (auto n = cells_.find({id.x, id.y - 1}); n != cells_.end())
		{
			for (int x = 0; x <= noise_per_cell; ++x)
			{
				c.noise_values[x] = n->second.noise_values[x + noise_per_cell * (noise_per_cell + 1)];
			}
		}

		if (auto n = cells_.find({id.x, id.y + 1}); n != cells_.end())
		{
			for (int x = 0; x <= noise_per_cell; ++x)
			{
				c.noise_values[x + noise_per_cell * (noise_per_cell + 1)] = n->second.noise_values[x];
			}
		}

		for (int x = 0; x <= noise_per_cell; ++x)
		{
			for (int y = 0; y <= noise_per_cell; ++y)
			{
				float rx = id.x * cell_size + x * cell_size / noise_per_cell;
				float ry = id.y * cell_size + y * cell_size / noise_per_cell;

				if (rx * rx + ry * ry <= 2500.f)
				{
					c.noise_values[x + y * (noise_per_cell + 1)] = 0.f;
				}
			}
		}

		for (int x = 0; x < noise_per_cell; ++x)
		{
			for (int y = 0; y < noise_per_cell; ++y)
			{
				for (int ix = 0; ix <= tiles_per_noise; ++ix)
				{
					for (int iy = 0; iy <= tiles_per_noise; ++iy)
					{
						float tx = static_cast<float>(ix) / tiles_per_noise;
						float ty = static_cast<float>(iy) / tiles_per_noise;

						float vx0 = lerp(c.noise_values[x     + y * (noise_per_cell + 1)], c.noise_values[x + 1 + y * (noise_per_cell + 1)], tx);

						float vx1 = lerp(c.noise_values[x     + (y + 1) * (noise_per_cell + 1)], c.noise_values[x + 1 + (y + 1) * (noise_per_cell + 1)], tx);

						float v = lerp(vx0, vx1, ty);

						c.values[x * tiles_per_noise + ix + (y * tiles_per_noise + iy) * (tiles_per_cell + 1)] = v;
					}
				}
			}
		}

		// TODO: consider using perlin noise instead
	}

	if (c.mesh_needs_update)
	{
		if (!c.mesh)
		{
			c.mesh.emplace();
			c.mesh->setup<vec2, color>(gl::TRIANGLES);
		}

		struct vertex
		{
			vec2 pos;
			color c;
		};

		std::vector<vertex> vertices;

		auto visit_triangle = [&](float x0, float y0, float v0, float x1, float y1, float v1, float x2, float y2, float v2)
		{
			int mask = 0;
			if (v0 > threshold) mask |= 1;
			if (v1 > threshold) mask |= 2;
			if (v2 > threshold) mask |= 4;

			vec2 p[3];
			p[0] = {x0, y0};
			p[1] = {x1, y1};
			p[2] = {x2, y2};

			for (auto & q : p) q = q * tile_size;

			float v[3] = { v0, v1, v2 };

			auto get_color = [](float v)
			{
				auto r = static_cast<std::uint8_t>(160 - v * 128);
				auto g = static_cast<std::uint8_t>(64 * (1.f - v));
				auto b = static_cast<std::uint8_t>(0);
				return color{ r, g, b, 255 };
			};

			auto push = [&](int i)
			{
				vertices.push_back({p[i], get_color((v[i] - threshold) / (1.f - threshold))});
			};

			auto pushm = [&](int i, int j)
			{
				float t = (threshold - v[i]) / (v[j] - v[i]);
				vertices.push_back({p[i] * (1.f - t) + p[j] * t, get_color(0.f)});
			};

			auto push3 = [&](int i)
			{
				push(i);
				pushm(i, (i + 1) % 3);
				pushm(i, (i + 2) % 3);
			};

			auto push4 = [&](int i)
			{
				push((i + 1) % 3);
				push((i + 2) % 3);
				pushm(i, (i + 1) % 3);
				pushm(i, (i + 1) % 3);
				push((i + 2) % 3);
				pushm(i, (i + 2) % 3);
			};

			switch (mask)
			{
			case 0b000:
				break;
			case 0b001:
				push3(0);
				break;
			case 0b010:
				push3(1);
				break;
			case 0b011:
				push4(2);
				break;
			case 0b100:
				push3(2);
				break;
			case 0b101:
				push4(1);
				break;
			case 0b110:
				push4(0);
				break;
			case 0b111:
				push(0);
				push(1);
				push(2);
				break;
			default:
				break;
			}
		};

		for (int x = 0; x < tiles_per_cell; ++x)
		{
			for (int y = 0; y < tiles_per_cell; ++y)
			{
				int tx = id.x * tiles_per_cell + x;
				int ty = id.y * tiles_per_cell + y;

				float v00 = c.values[x + y * (tiles_per_cell + 1)];
				float v10 = c.values[(x + 1) + y * (tiles_per_cell + 1)];
				float v01 = c.values[x + (y + 1) * (tiles_per_cell + 1)];
				float v11 = c.values[(x + 1) + (y + 1) * (tiles_per_cell + 1)];

				visit_triangle(tx, ty, v00, tx + 1, ty, v10, tx + 1, ty + 1, v11);
				visit_triangle(tx, ty, v00, tx, ty + 1, v01, tx + 1, ty + 1, v11);
			}
		}

		c.mesh->load(vertices);

		c.mesh_needs_update = false;
	}

	return c;
}

struct explosions
{
	explosions();

	void update(float dt);
	void render(vec2 const & camera_pos, float camera_scale, float aspect_ratio);
	void explode(vec2 pos, float radius, color col);

	void reset()
	{
		explosions_.clear();
	}

private:
	program program_;
	mesh mesh_;

	struct explosion
	{
		vec2 pos;
		float radius;
		color col;
		float status;
	};

	std::vector<explosion> explosions_;
};

explosions::explosions()
	: program_{colored_world_vertex_glsl, colored_world_fragment_glsl, "explosions"}
{
	mesh_.setup<vec2, color>(gl::TRIANGLES);
}

void explosions::update(float dt)
{
	std::vector<explosion> live_explosions;

	for (auto & e : explosions_)
	{
		e.status += dt;
		if (e.status < 1.f)
		{
			live_explosions.push_back(e);
		}
	}

	explosions_ = std::move(live_explosions);
}

void explosions::render(vec2 const & camera_pos, float camera_scale, float aspect_ratio)
{
	struct vertex
	{
		vec2 pos;
		color col;
	};

	std::vector<vertex> vertices;

	for (auto const & e : explosions_)
	{
		float r = e.radius * e.status;

		color c = e.col;
		c.a *= (1.f - e.status);

		int n = 12;
		for (int i = 0; i < n; ++i)
		{
			float a = (i * 2.f * pi) / n;
			float b = ((i + 1) * 2.f * pi) / n;

			vertices.push_back({e.pos, color{255, 255, 255, 0}});
			vertices.push_back({e.pos + dir(a) * r, c});
			vertices.push_back({e.pos + dir(b) * r, c});
		}
	}

	mesh_.load(vertices, gl::STREAM_DRAW);

	program_.use();
	program_.uniform("camera_pos", camera_pos);
	program_.uniform("camera_scale", camera_scale);
	program_.uniform("aspect_ratio", aspect_ratio);
	mesh_.draw();
}

void explosions::explode(vec2 pos, float radius, color col)
{
	explosions_.push_back({pos, radius, col, 0.f});
}

struct bars
{
	float health = 1.f;
	float rewind = 0.f;
	float experience = 0.f;

	bars();

	void render();

private:
	program program_;
	mesh mesh_;
};

bars::bars()
	: program_{colored_screen_vertex_glsl, colored_screen_fragment_glsl, "bar"}
{
	mesh_.setup<vec2, color>(gl::TRIANGLES);
}

void bars::render()
{
	struct vertex
	{
		vec2 pos;
		color col;
	};

	std::vector<vertex> vertices;

	auto push_bar = [&](float left, float right, float bottom, float top, float value, color const & col)
	{
		color bg = col;
		bg.r /= 4;
		bg.g /= 4;
		bg.b /= 4;

		vertices.push_back({{left, bottom}, bg});
		vertices.push_back({{right, bottom}, bg});
		vertices.push_back({{right, top}, bg});
		vertices.push_back({{left, bottom}, bg});
		vertices.push_back({{left, top}, bg});
		vertices.push_back({{right, top}, bg});

		right = left + (right - left) * value;

		vertices.push_back({{left, bottom}, col});
		vertices.push_back({{right, bottom}, col});
		vertices.push_back({{right, top}, col});
		vertices.push_back({{left, bottom}, col});
		vertices.push_back({{left, top}, col});
		vertices.push_back({{right, top}, col});
	};

	push_bar(-0.9f, -0.1f, -0.9f, -0.85f, health, {255, 0, 0, 255});
	push_bar( 0.9f,  0.1f, -0.9f, -0.85f, rewind, {0, 0, 255, 255});
	push_bar(-0.9f,  0.9f, 0.925f, 0.95f, experience, {0, 255, 0, 255});

	mesh_.load(vertices, gl::STREAM_DRAW);

	program_.use();
	mesh_.draw();
}

struct cannons;

struct bullets
{
	bullets();

	struct bullet
	{
		vec2 pos;
		vec2 vel;
		float length;
		color col;
	};

	void fire(vec2 pos, vec2 vel, float length, color col);
	void update(float dt, vec2 const & ship_pos);
	void collide(terrain & t, explosions & e, sound & s);

	void render(vec2 const & camera_pos, float camera_scale, float aspect_ratio);

	void reset()
	{
		bullets_.clear();
	}

private:
	program program_;
	mesh mesh_;

	std::vector<bullet> bullets_;

	friend float collide(bullets & bul, cannons & can, explosions & e, stats & s, sound & snd);
};

bullets::bullets()
	: program_{colored_world_vertex_glsl, colored_world_fragment_glsl, "bullets"}
{
	mesh_.setup<vec2, color>(gl::LINES);
}

void bullets::fire(vec2 pos, vec2 vel, float length, color col)
{
	bullets_.push_back({pos, vel, length, col});
}

void bullets::update(float dt, vec2 const & ship_pos)
{
	std::vector<bullet> live_bullets;

	for (auto & b : bullets_)
	{
		if (distance(b.pos, ship_pos) < 500.f)
		{
			b.pos += b.vel * dt;
			live_bullets.push_back(b);
		}
	}

	bullets_ = std::move(live_bullets);
}

void bullets::collide(terrain & t, explosions & e, sound & s)
{
	std::vector<bullet> live_bullets;

	for (auto & b : bullets_)
	{
		if (t.value(b.pos) >= t.threshold)
		{
			t.explode(b.pos, 8.f, 0.1f);
			e.explode(b.pos, 4.f, {0, 255, 0, 255});
			s.play_explosion();
		}
		else
		{
			live_bullets.push_back(b);
		}
	}

	bullets_ = std::move(live_bullets);
}

void bullets::render(vec2 const & camera_pos, float camera_scale, float aspect_ratio)
{
	struct vertex
	{
		vec2 pos;
		color col;
	};

	std::vector<vertex> vertices;

	for (auto const & b : bullets_)
	{
		auto const d = normalized(b.vel) * b.length;
		vertices.push_back({b.pos - d, {b.col.r, b.col.g, b.col.b, 0}});
		vertices.push_back({b.pos + d, b.col});
	}

	mesh_.load(vertices, gl::STREAM_DRAW);

	program_.use();
	program_.uniform("camera_pos", camera_pos);
	program_.uniform("camera_scale", camera_scale);
	program_.uniform("aspect_ratio", aspect_ratio);
	mesh_.draw();
}

struct cannons
{
	cannons(random_engine & rng);

	void set_main_menu(bool m, float camera_scale, float aspect_ratio);

	void generate(vec2 const & ship_pos, terrain const & ter, int level);
	void fire(vec2 const & ship_pos, int level);
	void update(float dt, vec2 const & ship_pos, terrain const & t);
	std::optional<vec2> collide(vec2 const & ship_pos, explosions & e, sound & snd);
	void collide(terrain & ter, explosions & e, sound & snd);
	float nearest_rocket(vec2 const & ship_pos);

	void render(vec2 const & camera_pos, float camera_scale, float aspect_ratio);
	void render_glow(vec2 const & camera_pos, float camera_scale, float aspect_ratio);

	void reset()
	{
		cannons_.clear();
		rockets_.clear();
	}

private:
	random_engine & rng_;

	bool in_main_menu_ = false;

	program program_;
	mesh mesh_;
	mesh rockets_mesh_;

	struct cannon
	{
		vec2 pos;
		float reload;
		float angle;
		bool rotate_left;
	};

	std::vector<cannon> cannons_;

	struct rocket
	{
		vec2 pos;
		float angle;
		float speed;
		float initial_cooldown;
	};

	std::vector<rocket> rockets_;

	friend float collide(bullets & bul, cannons & can, explosions & e, stats & s, sound & snd);
};

cannons::cannons(random_engine & rng)
	: rng_(rng)
	, program_{colored_world_vertex_glsl, colored_world_fragment_glsl, "cannons"}
{
	mesh_.setup<vec2, color>(gl::TRIANGLES);
	rockets_mesh_.setup<vec2, color>(gl::TRIANGLES);
}

void cannons::set_main_menu(bool m, float camera_scale, float aspect_ratio)
{
	if (m != in_main_menu_)
	{
		cannons_.clear();
		rockets_.clear();

		if (m)
		{
			vec2 c = vec2{0.1f, -0.35f} * camera_scale;
			vec2 r = vec2{0.1f, -0.45f} * camera_scale;
			cannons_.push_back({c, 0.0f, 0.0f, true});
			rockets_.push_back({r, 0.f, 50.f, 0.f});
		}
	}

	in_main_menu_ = m;
}

void cannons::generate(vec2 const & ship_pos, terrain const & ter, int level)
{
	int count_near = 0;
	for (auto const & c : cannons_)
	{
		if (std::abs(c.pos.x - ship_pos.x) <= 100.f
			&& std::abs(c.pos.y - ship_pos.y) <= 100.f)
		{
			++count_near;
		}
	}

	if (count_near < level)
	{
		for (int attempt = 0; attempt < 10; ++attempt)
		{
			vec2 pos;

			float left = ship_pos.x - 100.f;
			float right = ship_pos.x + 100.f;
			float bottom = ship_pos.y - 100.f;
			float top = ship_pos.y + 100.f;

			pos.x = rng_(left, right);
			pos.y = rng_(bottom, top);

			if (ter.value(pos) + 0.2f < ter.threshold)
			{
				cannons_.push_back({pos, 0.f, rng_(0.f, 2.f * pi), rng_.integer(2) == 0});
				break;
			}
		}
	}
}

void cannons::fire(vec2 const & ship_pos, int level)
{
	for (auto & c : cannons_)
	{
		if (std::abs(c.pos.x - ship_pos.x) <= 150.f
			&& std::abs(c.pos.y - ship_pos.y) <= 150.f)
		{
			if (c.reload <= 0.f)
			{
				rockets_.push_back({c.pos, rng_(0.f, 2.f * pi), 50.f + 2.f * level, 0.3f});
				c.reload = 2.5f / (4 + level);
			}
		}
	}
}

void cannons::update(float dt, vec2 const & ship_pos, terrain const & t)
{
	for (auto & c : cannons_)
	{
		if (c.reload >= 0.f)
			c.reload -= dt;

		if (c.rotate_left)
			c.angle += 2.f * dt;
		else
			c.angle -= 2.f * dt;
	}

	if (in_main_menu_)
	{
		for (auto & r : rockets_)
		{
		}
	}
	else
	{
		for (auto & r : rockets_)
		{
			r.initial_cooldown -= dt;
			if (r.initial_cooldown <= 0.f)
			{
				auto d = dir(r.angle);

				if (det(d, ship_pos - r.pos) > 0.f)
				{
					r.angle += 4.f * dt;
				}
				else
				{
					r.angle -= 4.f * dt;
				}
			}

			auto vel = dir(r.angle) * r.speed;
			r.pos += vel * dt;
		}
	}
}

std::optional<vec2> cannons::collide(vec2 const & ship_pos, explosions & e, sound & snd)
{
	std::optional<vec2> collision;

	std::vector<rocket> live_rockets;
	for (auto const & r : rockets_)
	{
		if (distance(r.pos, ship_pos) <= 1.f)
		{
			collision = dir(r.angle);
			e.explode(r.pos, 8.f, {0, 0, 255, 255});
			snd.play_explosion();
		}
		else
		{
			live_rockets.push_back(r);
		}
	}
	rockets_ = std::move(live_rockets);

	return collision;
}

void cannons::collide(terrain & ter, explosions & e, sound & snd)
{
	std::vector<rocket> live_rockets;
	for (auto const & r : rockets_)
	{
		if (ter.value(r.pos) >= ter.threshold)
		{
			ter.explode(r.pos, 4.f, 0.5f);
			e.explode(r.pos, 4.f, {255, 0, 0, 255});
			snd.play_explosion();
		}
		else
		{
			live_rockets.push_back(r);
		}
	}
	rockets_ = std::move(live_rockets);
}

float cannons::nearest_rocket(vec2 const & ship_pos)
{
	float min = std::numeric_limits<float>::infinity();

	for (auto const & r : rockets_)
	{
		float d = distance(r.pos, ship_pos);
		if (d < min) min = d;
	}

	return min;
}

void cannons::render(vec2 const & camera_pos, float camera_scale, float aspect_ratio)
{
	struct vertex
	{
		vec2 pos;
		color col;
	};

	std::vector<vertex> vertices;

	for (auto const & c : cannons_)
	{
		int const n = 6;
		for (int i = 0; i < n; ++i)
		{
			float a = (i * 2.f * pi) / n + c.angle;
			float b = ((i + 1) * 2.f * pi) / n + c.angle;

			float R = 2.f;
			float r = 1.5f;

			vertices.push_back({c.pos, {191, 191, 191, 255}});
			vertices.push_back({c.pos + dir(a) * R, {191, 191, 191, 255}});
			vertices.push_back({c.pos + dir(b) * R, {191, 191, 191, 255}});

			vertices.push_back({c.pos, {95, 95, 95, 255}});
			vertices.push_back({c.pos + dir(a) * r, {95, 95, 95, 255}});
			vertices.push_back({c.pos + dir(b) * r, {95, 95, 95, 255}});
		}
	}

	mesh_.load(vertices);

	for (auto const & r : rockets_)
	{
		auto d = dir(r.angle);
		auto n = rot90(d);

		if (r.initial_cooldown <= 0.f)
		{
			vertices.push_back({r.pos - d * 2.f + vec2{rng_(-0.2f, 0.2f), rng_(-0.2f, 0.2f)}, {255, 255, 0, 255}});
			vertices.push_back({r.pos - d + n * 0.4f, {255, 255, 0, 0}});
			vertices.push_back({r.pos - d - n * 0.4f, {255, 255, 0, 0}});
		}

		vertices.push_back({r.pos, {255, 255, 255, 255}});
		vertices.push_back({r.pos - d + n * 0.3f, {255, 255, 255, 255}});
		vertices.push_back({r.pos - d * 1.4f + n * 0.8f, {255, 255, 255, 255}});
		vertices.push_back({r.pos, {255, 255, 255, 255}});
		vertices.push_back({r.pos - d - n * 0.3f, {255, 255, 255, 255}});
		vertices.push_back({r.pos - d * 1.4f - n * 0.8f, {255, 255, 255, 255}});

		vertices.push_back({r.pos + d, {255, 0, 0, 255}});
		vertices.push_back({r.pos + d * 0.3f + n * 0.4f, {255, 0, 0, 255}});
		vertices.push_back({r.pos + d * 0.3f - n * 0.4f, {255, 0, 0, 255}});

		vertices.push_back({r.pos + d * 0.3f - n * 0.4f, {127, 127, 127, 255}});
		vertices.push_back({r.pos + d * 0.3f + n * 0.4f, {127, 127, 127, 255}});
		vertices.push_back({r.pos - d        + n * 0.3f, {127, 127, 127, 255}});
		vertices.push_back({r.pos + d * 0.3f - n * 0.4f, {127, 127, 127, 255}});
		vertices.push_back({r.pos - d        + n * 0.3f, {127, 127, 127, 255}});
		vertices.push_back({r.pos - d        - n * 0.3f, {127, 127, 127, 255}});
	}

	rockets_mesh_.load(vertices, gl::STREAM_DRAW);

	program_.use();
	program_.uniform("camera_pos", camera_pos);
	program_.uniform("camera_scale", camera_scale);
	program_.uniform("aspect_ratio", aspect_ratio);
	mesh_.draw();
	rockets_mesh_.draw();
}

void cannons::render_glow(vec2 const & camera_pos, float camera_scale, float aspect_ratio)
{
	struct vertex
	{
		vec2 pos;
		color col;
	};

	std::vector<vertex> vertices;

	for (auto const & r : rockets_)
	{
		auto d = dir(r.angle);
		auto n = rot90(d);

		if (r.initial_cooldown <= 0.f)
		{
			vertices.push_back({r.pos - d * 2.f + vec2{rng_(-0.2f, 0.2f), rng_(-0.2f, 0.2f)}, {255, 255, 0, 255}});
			vertices.push_back({r.pos - d + n * 0.4f, {255, 255, 0, 0}});
			vertices.push_back({r.pos - d - n * 0.4f, {255, 255, 0, 0}});
		}

		vertices.push_back({r.pos + d, {255, 0, 0, 255}});
		vertices.push_back({r.pos + d * 0.3f + n * 0.4f, {255, 0, 0, 255}});
		vertices.push_back({r.pos + d * 0.3f - n * 0.4f, {255, 0, 0, 255}});
	}

	rockets_mesh_.load(vertices, gl::STREAM_DRAW);

	program_.use();
	program_.uniform("camera_pos", camera_pos);
	program_.uniform("camera_scale", camera_scale);
	program_.uniform("aspect_ratio", aspect_ratio);
	rockets_mesh_.draw();
}

float collide(bullets & bul, cannons & can, explosions & e, stats & s, sound & snd)
{
	std::set<std::size_t> dead_bullet_idx;
	std::set<std::size_t> dead_rocket_idx;
	std::set<std::size_t> dead_cannon_idx;

	float experience = 0.f;

	for (std::size_t j1 = 0; j1 < can.rockets_.size(); ++j1)
	{
		if (dead_rocket_idx.count(j1) > 0) continue;

		for (std::size_t j2 = j1 + 1; j2 < can.rockets_.size(); ++j2)
		{
			if (dead_rocket_idx.count(j2) > 0) continue;

			if (distance(can.rockets_[j2].pos, can.rockets_[j1].pos) < 2.f)
			{
				dead_rocket_idx.insert(j1);
				dead_rocket_idx.insert(j2);
				e.explode(can.rockets_[j2].pos, 4.f, {255, 0, 0, 255});
				snd.play_explosion();
				break;
			}
		}
	}

	for (std::size_t i = 0; i < bul.bullets_.size(); ++i)
	{
		if (dead_bullet_idx.count(i) > 0) continue;

		for (std::size_t j = 0; j < can.rockets_.size(); ++j)
		{
			if (dead_rocket_idx.count(j) > 0) continue;

			if (distance(bul.bullets_[i].pos, can.rockets_[j].pos) < 3.f)
			{
				dead_bullet_idx.insert(i);
				dead_rocket_idx.insert(j);

				e.explode((bul.bullets_[i].pos + can.rockets_[j].pos) / 2.f, 4.f, {255, 127, 0, 255});

				snd.play_explosion();

				++s.rockets;

				experience += 1.f;
				break;
			}
		}
	}

	for (std::size_t i = 0; i < bul.bullets_.size(); ++i)
	{
		if (dead_bullet_idx.count(i) > 0) continue;

		for (std::size_t j = 0; j < can.cannons_.size(); ++j)
		{
			if (dead_cannon_idx.count(j) > 0) continue;

			if (distance(bul.bullets_[i].pos, can.cannons_[j].pos) < 4.f)
			{
				dead_bullet_idx.insert(i);
				dead_cannon_idx.insert(j);

				e.explode(bul.bullets_[i].pos, 4.f, {0, 255, 0, 255});
				e.explode(can.cannons_[j].pos, 16.f, {255, 255, 255, 255});

				snd.play_explosion();

				++s.cannons;

				experience += 5.f;
				break;
			}
		}
	}

	std::vector<bullets::bullet> live_bullets;
	for (std::size_t i = 0; i < bul.bullets_.size(); ++i)
		if (dead_bullet_idx.count(i) == 0)
			live_bullets.push_back(bul.bullets_[i]);
	bul.bullets_ = std::move(live_bullets);

	std::vector<cannons::rocket> live_rockets;
	for (std::size_t j = 0; j < can.rockets_.size(); ++j)
		if (dead_rocket_idx.count(j) == 0)
			live_rockets.push_back(can.rockets_[j]);
	can.rockets_ = std::move(live_rockets);

	std::vector<cannons::cannon> live_cannons;
	for (std::size_t j = 0; j < can.cannons_.size(); ++j)
		if (dead_cannon_idx.count(j) == 0)
			live_cannons.push_back(can.cannons_[j]);
	can.cannons_ = std::move(live_cannons);

	return experience;
}

int main() try
{
	SDL_Init(SDL_INIT_VIDEO);

	int window_width = 800;
	int window_height = 600;

	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 1);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	sdl_ptr<SDL_Window> window { SDL_CreateWindow("AstroWind", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		window_width, window_height, SDL_WINDOW_RESIZABLE | SDL_WINDOW_FULLSCREEN_DESKTOP | SDL_WINDOW_BORDERLESS | SDL_WINDOW_OPENGL),
		SDL_DestroyWindow };

	if (!window)
		sdl_fail("Failed to create window: ");

	SDL_ShowCursor(SDL_FALSE);

	SDL_GetWindowSize(window.get(), &window_width, &window_height);

	sdl_ptr<void> gl_context { SDL_GL_CreateContext(window.get()), SDL_GL_DeleteContext };

	if (!gl_context)
		sdl_fail("Failed to create OpenGL 3.3 core context: ");

	SDL_GL_SetSwapInterval(1);

	if (!gl::sys::LoadFunctions())
		fail("Failed to load OpenGL functions");

	gl::Enable(gl::BLEND);
	gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);

	random_engine rng;

	vec2 camera_pos { 0.f, 0.f };
	float camera_scale = window_height / 15.f;
	float camera_scale_tgt = camera_scale;
	float camera_shatter = 0.f;

	struct text text;
	struct stars stars(rng);
	struct bars bars;

	struct ship ship(rng);
	struct bullets bullets;
	struct terrain terrain(rng);
	struct cannons cannons(rng);
	struct explosions explosions;
	struct stats stats;

	struct stats records;
	struct stats new_records;

	struct sound sound(rng);

#ifdef NDEBUG
	records.read();
#endif

	int mode = 0;

	std::unordered_map<SDL_Keycode, bool> key_down;

	bool rewind = false;
	float rewind_duration = 0.f;

	bool dead = false;
	float death_status = 0.f;

	program death_program{colored_screen_vertex_glsl, colored_screen_fragment_glsl, "death"};
	mesh death_mesh;
	death_mesh.setup<vec2, color>(gl::TRIANGLES);

	int const blur_downscale = 2;

	post_processing_effect glow_base(textured_screen_vertex_glsl, textured_screen_fragment_glsl, "glow base");
	glow_base.resize(window_width / blur_downscale, window_height / blur_downscale);

	post_processing_effect gaussian_blur_horizontal(gaussian_blur_horizontal_vertex_glsl, gaussian_blur_fragment_glsl, "gaussian blur horizontal");
	gaussian_blur_horizontal.resize(glow_base.width(), glow_base.height());

	post_processing_effect gaussian_blur_vertical(gaussian_blur_vertical_vertex_glsl, gaussian_blur_fragment_glsl, "gaussian blur vertical");
	gaussian_blur_vertical.resize(glow_base.width(), glow_base.height());

	program textured_screen{textured_screen_vertex_glsl, textured_screen_fragment_glsl, "textured screen"};

	mesh textured_screen_mesh;
	textured_screen_mesh.setup<vec2, vec2>(gl::TRIANGLES);
	{
		struct vertex
		{
			vec2 pos;
			vec2 texcoord;
		};

		std::vector<vertex> vertices
		{
			{{-1.f, -1.f}, {0.f, 0.f}},
			{{ 1.f, -1.f}, {1.f, 0.f}},
			{{ 1.f,  1.f}, {1.f, 1.f}},
			{{-1.f, -1.f}, {0.f, 0.f}},
			{{ 1.f,  1.f}, {1.f, 1.f}},
			{{-1.f,  1.f}, {0.f, 1.f}},
		};

		textured_screen_mesh.load(vertices);
	}

	bool paused = false;
	bool pause_press_handled = false;

	using timer_clock = std::chrono::system_clock;

	std::optional<timer_clock::time_point> start_time;

	using fps_clock = std::chrono::high_resolution_clock;

	std::optional<fps_clock::time_point> last_frame_start;

	std::vector<std::string> messages =
	{
		"YOU'RE BREATHTAKING!",
		"WEAR A MASK!",
		"DON'T STOP SHOOTING!",
		"KEEP MOVING!",
		"USE REWIND!",
	};

	int message = rng.integer(messages.size());

	float dt = 0.f;

	float const ship_rotation_speed = 10.f;
	float const ship_max_boost = 200.f;

	bool transition_to_mode_1 = false;

	for (bool running = true; running;)
	{
		for (SDL_Event event; SDL_PollEvent(&event);) switch (event.type)
		{
		case SDL_QUIT:
			running = false;
			break;
		case SDL_WINDOWEVENT:
			switch (event.window.event)
			{
			case SDL_WINDOWEVENT_RESIZED:
				window_width = event.window.data1;
				window_height = event.window.data2;
				glow_base.resize(window_width / blur_downscale, window_height / blur_downscale);
				gaussian_blur_horizontal.resize(glow_base.width(), glow_base.height());
				gaussian_blur_vertical.resize(glow_base.width(), glow_base.height());
				break;
			case SDL_WINDOWEVENT_FOCUS_LOST:
			case SDL_WINDOWEVENT_HIDDEN:
				if (mode == 1) paused = true;
				break;
			}
			break;
		case SDL_KEYDOWN:
			key_down[event.key.keysym.sym] = true;
			break;
		case SDL_KEYUP:
			key_down[event.key.keysym.sym] = false;
			break;
		}

		if (key_down[SDLK_ESCAPE])
			running = false;

		if (!running) break;

		framebuffer::bind_default();
		gl::Viewport(0, 0, window_width, window_height);

		text.reset();

		float const aspect_ratio = static_cast<float>(window_width) / window_height;

		auto this_frame_start = fps_clock::now();

		if (last_frame_start)
		{
			dt = std::chrono::duration_cast<std::chrono::duration<float>>(this_frame_start - *last_frame_start).count() * 60.f / 100.f;
		}

		last_frame_start = this_frame_start;

		if (mode == 0)
		{
			camera_scale = window_height / 15.f;

			cannons.set_main_menu(true, camera_scale, aspect_ratio);

			float ship_boost = 0.f;

			vec2 ship_direction = dir(ship.angle);

			if (transition_to_mode_1)
			{
				while (ship.angle > pi)
					ship.angle -= pi;
				while (ship.angle < -pi)
					ship.angle += pi;

				ship.angle_tgt = 0.f;

				ship.update(dt, 2.f * ship_max_boost);
			}
			else
			{
				if (key_down[SDLK_DOWN] || key_down[SDLK_s])
				{
					// braking auto-pilot

					auto brake_dir = normalized(ship.vel);

					if (length(ship.vel) < 10.f)
						ship.vel = { 0.f, 0.f };

					if (dot(brake_dir, ship.vel) > 0.f)
					{
						float f = -dot(brake_dir, ship_direction);

						if (det(brake_dir, ship_direction) > 0.f)
							ship.angle_tgt += 2.f * (1.f - f) * ship_rotation_speed * dt;
						else
							ship.angle_tgt -= 2.f * (1.f - f) * ship_rotation_speed * dt;

						if (f > 0.8f)
							ship_boost = std::min(2.f * f * ship_max_boost, length(ship.vel) / dt);
					}
				}
				else
				{
					if (key_down[SDLK_RIGHT] || key_down[SDLK_d])
						ship.angle_tgt -= ship_rotation_speed * dt;

					if (key_down[SDLK_LEFT] || key_down[SDLK_a])
						ship.angle_tgt += ship_rotation_speed * dt;

					if (key_down[SDLK_UP] || key_down[SDLK_w])
						ship_boost = ship_max_boost;
				}

				ship.update(dt, ship_boost);
			}

			if (transition_to_mode_1 && (ship.pos.x > camera_scale * aspect_ratio))
			{
				mode = 1;
				key_down.clear();
				camera_pos = vec2{0.f, 0.f};
				ship.reset();
				cannons.set_main_menu(false, camera_scale, aspect_ratio);
			}

			{
				std::optional<vec2> n;

				if (ship.pos.x < -camera_scale * aspect_ratio)
				{
					n = vec2{1.f, 0.f};
					ship.pos.x = -camera_scale * aspect_ratio;
				}

				if (ship.pos.x >  camera_scale * aspect_ratio)
				{
					n = vec2{-1.f, 0.f};
					ship.pos.x = camera_scale * aspect_ratio;
				}

				if (ship.pos.y < -camera_scale)
				{
					n = vec2{0.f, 1.f};
					ship.pos.y = -camera_scale;
				}

				if (ship.pos.y >  camera_scale)
				{
					n = vec2{0.f, -1.f};
					ship.pos.y = camera_scale;
				}

				if (n)
				{
					float p = dot(ship.vel, *n);

					if (p < 0.f)
					{
						ship.vel -= 1.5f * (*n) * p;
					}
				}
			}

			cannons.update(dt, ship.pos, terrain);

			sound.propulsion_level(0.2f + 0.8f * ship.propulsion);

			gl::ClearColor(0.f, 0.0f, 0.0f, 1.f);
			gl::Clear(gl::COLOR_BUFFER_BIT);

			stars.render(camera_pos, camera_scale, aspect_ratio);

			cannons.render(vec2{0.f, 0.f}, camera_scale, aspect_ratio);

			ship.render(vec2{0.f, 0.f}, camera_scale, aspect_ratio);

			text.add(messages[message], {-0.95f, 0.9f}, -1, 1, {127, 127, 127, 255});

			text.add("ASTROWIND", {0.f, 0.7f}, 0, 0, {0, 255, 255, 255});

			text.add("ARROWS/WASD: MOVE", {0.f, 0.5f}, 0, 0, {255, 255, 255, 255});
			text.add("[SPACE]: SHOOT", {0.f, 0.4f}, 0, 0, {255, 255, 255, 255});
			text.add("HOLD [R]: REWIND", {0.f, 0.3f}, 0, 0, {255, 255, 255, 255});
			text.add("[P]: PAUSE", {0.f, 0.2f}, 0, 0, {255, 255, 255, 255});
			text.add("[ESC]: EXIT", {0.f, 0.1f}, 0, 0, {255, 255, 255, 255});

			if (!transition_to_mode_1)
				text.add("PRESS [SPACE] TO CONTINUE", {0.f, -0.1f}, 0, 0, {0, 255, 0, 255});

			text.add("ENEMY:", {0.f, -0.4f}, 1, 0, {255, 0, 0, 255});

			text.add("MADE FOR BRACKEYS JAM 2020.2", {0.f, -0.8f}, 0, -1, {255, 255, 0, 255});
			text.add("BY LISYARUS", {0.f, -0.9f}, 0, -1, {255, 255, 0, 255});

			text.render(aspect_ratio);

			glow_base.bind();
			gl::ClearColor(0.f, 0.f, 0.f, 0.f);
			gl::Clear(gl::COLOR_BUFFER_BIT);
			text.render(aspect_ratio);
			cannons.render_glow(vec2{0.f, 0.f}, camera_scale, aspect_ratio);

			gaussian_blur_horizontal.execute(glow_base.out_texture(), glow_base.width(), glow_base.height());
			gaussian_blur_vertical.execute(gaussian_blur_horizontal.out_texture(), gaussian_blur_horizontal.width(), gaussian_blur_horizontal.height());

			framebuffer::bind_default();
			gl::Viewport(0, 0, window_width, window_height);

			textured_screen.use();
			textured_screen.uniform("tex", 0);
			gaussian_blur_vertical.out_texture().bind();
			textured_screen_mesh.draw();

			camera_pos.x += dt * 30.f;

			if (key_down[SDLK_SPACE])
			{
				transition_to_mode_1 = true;
			}
		}
		else if (mode == 1)
		{
			if (!start_time)
			{
				start_time = timer_clock::now();
			}

			float ship_boost = 0.f;

			vec2 ship_direction = dir(ship.angle);

			int level = std::floor(ship.experience);

			float dt_multiplier = 1.f;

			if (dead)
			{
				death_status += 2.f * dt;
			}
			else
			{
				if (key_down[SDLK_p])
				{
					if (!pause_press_handled)
					{
						paused = !paused;
						pause_press_handled = true;
					}
				}
				else
				{
					pause_press_handled = false;
				}

				if (!paused)
				{
					if (rewind && rewind_duration >= 3.f)
					{
						rewind = false;
						rewind_duration = 0.f;
						ship.rewind();
					}
					else if (key_down[SDLK_r] && ship.can_rewind())
					{
						if (!rewind) rewind_duration = 0.f;

						rewind = true;
						rewind_duration += dt;

						if (rewind_duration < 2.f)
							dt_multiplier = 0.2f;
						else if (rewind_duration < 3.f)
							dt_multiplier = 0.2f + (1.f - 0.2f) * (rewind_duration - 2.f) / (3.f - 2.f);
						else
							dt_multiplier = 1.f;
					}

					if (rewind)
						dt *= dt_multiplier;

					if (key_down[SDLK_DOWN] || key_down[SDLK_s])
					{
						// braking auto-pilot

						auto brake_dir = normalized(ship.vel);

						if (length(ship.vel) < 10.f)
							ship.vel = { 0.f, 0.f };

						if (dot(brake_dir, ship.vel) > 0.f)
						{
							float f = -dot(brake_dir, ship_direction);

							if (det(brake_dir, ship_direction) > 0.f)
								ship.angle_tgt += 2.f * (1.f - f) * ship_rotation_speed * dt;
							else
								ship.angle_tgt -= 2.f * (1.f - f) * ship_rotation_speed * dt;

							if (f > 0.8f)
								ship_boost = std::min(2.f * f * ship_max_boost, length(ship.vel) / dt);
						}
					}
					else
					{
						if (key_down[SDLK_RIGHT] || key_down[SDLK_d])
							ship.angle_tgt -= ship_rotation_speed * dt;

						if (key_down[SDLK_LEFT] || key_down[SDLK_a])
							ship.angle_tgt += ship_rotation_speed * dt;

						if (key_down[SDLK_UP] || key_down[SDLK_w])
							ship_boost = ship_max_boost;
					}

					if (!key_down[SDLK_r] && rewind)
					{
						ship.rewind();
						rewind = false;
					}

					if (key_down[SDLK_SPACE])
					{
						if (ship.fire_reload <= 0.f)
						{
							auto d = ship_direction;
							auto n = rot90(d);

							int buls = level;

							float range = 0.1f + level * 0.01f;

							float off = rng(- range * 0.5f, range * 0.5f);

							for (int i = 0; i < buls; ++i)
							{
								float a = off + (i - buls * 0.5f) / buls * 2.f * range;

								auto v = d * std::cos(a) + n * std::sin(a);

								bullets.fire(ship.pos + v, ship.vel + v * 200.f, 1.f, {0, 255, 0, 255});
							}

							stats.bullets += buls;

							ship.fire_reload = 0.2f / (1.f + 0.05f * level);

							sound.play_bullet();
						}
					}

					auto old_pos = ship.pos;
					ship.update(dt, ship_boost);
					stats.distance += distance(ship.pos, old_pos);

					if (rewind)
						camera_pos += (lerp(ship.pos, ship.rewind_pos(), 0.5f) - camera_pos) * 20.f * dt;
					else
						camera_pos += (ship.pos - camera_pos) * 20.f * dt;

					camera_pos += rng.unit_vector() * camera_shatter;

					{
						float t = length(ship.vel) / 100.f;
						t = std::min(1.f, t);
						camera_scale_tgt = slerp(window_height / 15.f, window_height / 10.f, t);
					}

					if (rewind)
						camera_scale_tgt /= (1.f + 0.2f * (dt - dt) / dt);

					camera_scale += (camera_scale_tgt - camera_scale) * 10.f * dt;

					cannons.generate(ship.pos, terrain, level);
					cannons.fire(ship.pos, level);
					cannons.update(dt, ship.pos, terrain);

					bullets.update(dt, ship.pos);

					bullets.collide(terrain, explosions, sound);
					cannons.collide(terrain, explosions, sound);
					if (auto c = cannons.collide(ship.pos, explosions, sound); c)
					{
						ship.health -= 0.1f;
						camera_shatter = std::max(camera_shatter, 1.f);
						ship.vel += *c * 25.f;
					}

					if (camera_shatter > 0.f)
						camera_shatter -= dt;

					ship.experience += 0.1f * collide(bullets, cannons, explosions, stats, sound) / level / level;

					if (float ft = terrain.value(ship.pos); ft >= terrain.threshold)
					{
						auto g = terrain.gradient(ship.pos);

						// one iteration of Newton's methid
						ship.pos += g * (terrain.threshold - ft) / dot(g, g);

						auto n = normalized(-g);

						float p = dot(ship.vel, n);

						if (p < 0.f)
						{
							ship.vel -= 1.5f * n * dot(ship.vel, n);
							camera_shatter = std::max(camera_shatter, -0.002f * p);
						}
					}

					if (ship.health < 0.f)
					{
						ship.health = 0.f;
						dead = true;
						rewind = false;
					}

					explosions.update(dt * 10.f);
				}
			}

			sound.pitch(dt_multiplier);

			if (!paused)
				sound.propulsion_level(0.2f + 0.8f * ship.propulsion);
			else
				sound.propulsion_level(0.f);

			ship.show_history = rewind;

			bars.health = ship.health;
			bars.rewind = rewind ? 1.f - rewind_duration / 3.f : ship.rewind_status();
			bars.experience = ship.experience - level;

			if (paused)
				glow_base.bind();

			if (rewind)
				gl::ClearColor(0.f, 0.0f, 0.1f * (1.f - dt_multiplier), 1.f);
			else
				gl::ClearColor(0.f, 0.0f, 0.0f, 1.f);
			gl::Clear(gl::COLOR_BUFFER_BIT);

			stars.render(camera_pos, camera_scale, aspect_ratio);
			terrain.render(camera_pos, camera_scale, aspect_ratio);
			cannons.render(camera_pos, camera_scale, aspect_ratio);
			ship.render(camera_pos, camera_scale, aspect_ratio);
			bullets.render(camera_pos, camera_scale, aspect_ratio);
			explosions.render(camera_pos, camera_scale, aspect_ratio);

			bars.render();

			text.add(to_string("LEVEL: ", level), {-0.9f, 0.9f}, -1, 1, {0, 255, 0, 255});
			text.add(to_string("EXPERIENCE: ", std::setw(2), static_cast<int>(bars.experience * 100), '%'), {0.9f, 0.9f}, 1, 1, {0, 255, 0, 255});
			text.add(to_string(' ', static_cast<int>(std::round(ship.health * 100))), {-0.1f, -0.9f}, -1, -1, {255, 0, 0, 255});
			text.add(to_string(static_cast<int>(ship.rewind_status() * 100), ' '), {0.1f, -0.9f}, 1, -1, {0, 0, 255, 255});

			text.render(aspect_ratio);

			if (paused)
			{
				gaussian_blur_horizontal.execute(glow_base.out_texture(), glow_base.width(), glow_base.height());
				gaussian_blur_vertical.execute(gaussian_blur_horizontal.out_texture(), gaussian_blur_horizontal.width(), gaussian_blur_horizontal.height());

				framebuffer::bind_default();
				gl::Viewport(0, 0, window_width, window_height);

				gl::Disable(gl::BLEND);

				textured_screen.use();
				textured_screen.uniform("tex", 0);
				gaussian_blur_vertical.out_texture().bind();
				textured_screen_mesh.draw();

				gl::Enable(gl::BLEND);

				text.reset();

				text.add("PAUSED", {0.f, 0.f}, 0, 0, {255, 255, 0, 255});
				text.add("PRESS [P] TO CONTINUE", {0.f, -0.1f}, 0, 0, {255, 255, 0, 255});

				text.render(aspect_ratio);
			}
			else
			{
				glow_base.bind();
				gl::ClearColor(0.f, 0.f, 0.f, 0.f);
				gl::Clear(gl::COLOR_BUFFER_BIT);
				text.render(aspect_ratio);
				bars.render();
				bullets.render(camera_pos, camera_scale, aspect_ratio);
				cannons.render_glow(camera_pos, camera_scale, aspect_ratio);

				gaussian_blur_horizontal.execute(glow_base.out_texture(), glow_base.width(), glow_base.height());
				gaussian_blur_vertical.execute(gaussian_blur_horizontal.out_texture(), gaussian_blur_horizontal.width(), gaussian_blur_horizontal.height());

				framebuffer::bind_default();
				gl::Viewport(0, 0, window_width, window_height);

				textured_screen.use();
				textured_screen.uniform("tex", 0);
				gaussian_blur_vertical.out_texture().bind();
				textured_screen_mesh.draw();
			}

			if (dead)
			{
				struct vertex
				{
					vec2 pos;
					color col;
				};

				std::uint8_t a = 255 * std::min(1.f, death_status);

				color c { 255, 0, 0, a };
				color r { 255, 0, 0, 0 };

				float R = 0.5f;

				std::vector<vertex> vertices;
				for (int a = 0; a < 8; ++a)
				{
					float t = a * pi / 4.f;
					float tt = (a + 1) * pi / 4.f;

					vec2 p = dir(t);
					vec2 q = dir(tt);

					p /= std::max(std::abs(p.x), std::abs(p.y));
					q /= std::max(std::abs(q.x), std::abs(q.y));

					vec2 p1 = p * R;
					vec2 q1 = q * R;

					vertices.push_back({p1, r});
					vertices.push_back({p, c});
					vertices.push_back({q, c});
					vertices.push_back({p1, r});
					vertices.push_back({q, c});
					vertices.push_back({q1, r});
				}
				death_mesh.load(vertices, gl::STREAM_DRAW);

				death_program.use();
				death_mesh.draw();
			}

			if (death_status >= 1.f)
			{
				stats.time_played = std::chrono::duration_cast<std::chrono::duration<int>>(timer_clock::now() - *start_time).count();
				start_time = std::nullopt;

				stats.kill_efficiency = (stats.bullets > 0) ? (stats.cannons + stats.rockets) * 100.f / stats.bullets : 0.f;

				stats.level = std::floor(ship.experience);

				new_records.time_played = std::max(records.time_played, stats.time_played);
				new_records.level = std::max(records.level, stats.level);
				new_records.distance = std::max(records.distance, stats.distance);
				new_records.bullets = std::max(records.bullets, stats.bullets);
				new_records.rockets = std::max(records.rockets, stats.rockets);
				new_records.cannons = std::max(records.cannons, stats.cannons);
				new_records.kill_efficiency = std::max(records.kill_efficiency, stats.kill_efficiency);

				new_records.write();

				sound.propulsion_level(0.f);
				sound.pitch(1.f);

				mode = 2;
			}
		}
		else if (mode == 2)
		{
			camera_scale = window_height / 15.f;

			gl::ClearColor(0.f, 0.0f, 0.0f, 1.f);
			gl::Clear(gl::COLOR_BUFFER_BIT);

			stars.render(camera_pos, camera_scale, aspect_ratio);

			text.add("YOU DIED!", {0.f, 0.8f}, 0, 0, {255, 0, 0, 255});

			text.add(to_string("TIME PLAYED: ", (stats.time_played / 60), ":", std::setw(2), std::setfill('0'), (stats.time_played % 60)), {0.f, 0.6f}, 0, 0, {255, 255, 255, 255});
			text.add(to_string("LEVEL: ", stats.level), {0.f, 0.5f}, 0, 0, {255, 255, 255, 255});
			text.add(to_string("DISTANCE FLOWN: ", static_cast<int>(stats.distance)), {0.f, 0.4f}, 0, 0, {255, 255, 255, 255});
			text.add(to_string("BULLETS SHOT: ", stats.bullets), {0.f, 0.3f}, 0, 0, {255, 255, 255, 255});
			text.add(to_string("ROCKETS KILLED: ", stats.rockets), {0.f, 0.2f}, 0, 0, {255, 255, 255, 255});
			text.add(to_string("STATIONS KILLED: ", stats.cannons), {0.f, 0.1f}, 0, 0, {255, 255, 255, 255});
			text.add(to_string("KILL EFFICIENCY: ", static_cast<int>(stats.kill_efficiency), "%"), {0.f, 0.f}, 0, 0, {255, 255, 255, 255});

			if (new_records.time_played > records.time_played)
				text.add("NEW RECORD!", {0.5f, 0.6f}, 0, 0, {255, 255, 0, 255});
			if (new_records.level > records.level)
				text.add("NEW RECORD!", {0.5f, 0.5f}, 0, 0, {255, 255, 0, 255});
			if (new_records.distance > records.distance)
				text.add("NEW RECORD!", {0.5f, 0.4f}, 0, 0, {255, 255, 0, 255});
			if (new_records.bullets > records.bullets)
				text.add("NEW RECORD!", {0.5f, 0.3f}, 0, 0, {255, 255, 0, 255});
			if (new_records.rockets > records.rockets)
				text.add("NEW RECORD!", {0.5f, 0.2f}, 0, 0, {255, 255, 0, 255});
			if (new_records.cannons > records.cannons)
				text.add("NEW RECORD!", {0.5f, 0.1f}, 0, 0, {255, 255, 0, 255});
			if (new_records.kill_efficiency > records.kill_efficiency)
				text.add("NEW RECORD!", {0.5f, 0.0f}, 0, 0, {255, 255, 0, 255});

			text.add("PRESS [SPACE] TO RESTART", {0.f, -0.2f}, 0, 0, {0, 255, 0, 255});
			text.add("PRESS [ESC] TO EXIT", {0.f, -0.3f}, 0, 0, {0, 255, 0, 255});
			text.render(aspect_ratio);

			glow_base.bind();
			gl::ClearColor(0.f, 0.f, 0.f, 0.f);
			gl::Clear(gl::COLOR_BUFFER_BIT);
			text.render(aspect_ratio);

			gaussian_blur_horizontal.execute(glow_base.out_texture(), glow_base.width(), glow_base.height());
			gaussian_blur_vertical.execute(gaussian_blur_horizontal.out_texture(), gaussian_blur_horizontal.width(), gaussian_blur_horizontal.height());

			framebuffer::bind_default();
			gl::Viewport(0, 0, window_width, window_height);

			textured_screen.use();
			textured_screen.uniform("tex", 0);
			gaussian_blur_vertical.out_texture().bind();
			textured_screen_mesh.draw();

			camera_pos.x += dt * 30.f;

			if (key_down[SDLK_SPACE])
			{
				mode = 1;
				key_down.clear();

				ship.reset();
				bullets.reset();
				terrain.reset();
				cannons.reset();
				explosions.reset();
				stats.reset();

				camera_pos = ship.pos;

				records = new_records;

				dead = false;
				death_status = 0.f;

				paused = false;

				rewind = false;

				camera_shatter = 0.f;
			}
		}

		SDL_GL_SwapWindow(window.get());
	}
}
catch (std::exception const & e)
{
	std::cerr << e.what() << std::endl;
	return EXIT_FAILURE;
}
catch (...)
{
	std::cerr << "Unknown exception" << std::endl;
	return EXIT_FAILURE;
}
