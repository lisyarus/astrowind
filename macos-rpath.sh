#!/usr/bin/env bash

install_name_tool -change "/usr/local/opt/sdl2/lib/libSDL2-2.0.0.dylib" "@rpath/libSDL2-2.0.0.dylib" $1
install_name_tool -add_rpath @executable_path/. $1
